import 'dart:convert';

class Servicios {
  int? id;
  int? idTipoServicio;
  int? idInscripcion;
  int? idClase;
  int? wifi;
  int? banoPrivado;
  int? banoCompartido;
  int? bedAndBreakfast;
  int? duchaCaliente;
  int? tarjetaCredito;
  int? tarjetaDebito;
  int? cocinaCompartida;
  int? livingRoom;
  int? lavanderia;
  int? accesoUniversal;
  int? areaAsadoFogon;
  int? menuEspecial;
  int? productosLocales;
  int? cervezaArtesanal;
  int? transporteActividad;
  int? equipamientoIncluido;
  int? equipamientoArriendo;
  int? snackIncluido;
  int? actividadGuiada;
  int? livOtro;
  int? cutRegion;
  String? livOtroTexto;
  String? nombres;
  String? apellidoPaterno;
  String? apellidoMaterno;
  String? razonSocial;
  String? nombreComercial;
  String? cutComuna;
  String? comuna;
  String? cutLocalidad;
  String? localidad;
  String? calle;
  String? numero;
  String? depto;
  String? otro;
  String? gpsx;
  String? gpsy;
  String? telefono;
  String? email;
  String? sitioWeb;

  Servicios({
        this.id,
        this.idInscripcion,
        this.idTipoServicio,
        this.nombres,
        this.apellidoPaterno,
        this.apellidoMaterno,
        this.razonSocial,
        this.nombreComercial,
        this.cutRegion,
        this.cutComuna,
        this.comuna,
        this.cutLocalidad,
        this.localidad,
        this.calle,
        this.numero,
        this.depto,
        this.otro,
        this.gpsx,
        this.gpsy,
        this.telefono,
        this.email,
        this.sitioWeb,
        this.idClase,
        this.wifi,
        this.banoPrivado,
        this.banoCompartido,
        this.bedAndBreakfast,
        this.duchaCaliente,
        this.tarjetaCredito,
        this.tarjetaDebito,
        this.cocinaCompartida,
        this.livingRoom,
        this.lavanderia,
        this.accesoUniversal,
        this.areaAsadoFogon,
        this.menuEspecial,
        this.productosLocales,
        this.cervezaArtesanal,
        this.transporteActividad,
        this.equipamientoIncluido,
        this.equipamientoArriendo,
        this.snackIncluido,
        this.actividadGuiada,
        this.livOtro,
        this.livOtroTexto
  });
  factory Servicios.fromJson(String str) => Servicios.fromMap(json.decode(str));

  factory Servicios.fromMap(Map<String, dynamic> json) => Servicios(
    id : json['id'],
    idInscripcion : json['id_inscripcion'],
    idTipoServicio : json['id_tipo_servicio'],
    nombres : json['nombres'],
    apellidoPaterno : json['apellido_paterno'],
    apellidoMaterno : json['apellido_materno'],
    razonSocial : json['razon_social'],
    nombreComercial : json['nombre_comercial'],
    cutRegion : json['cut_region'],
    cutComuna : json['cut_comuna'],
    comuna : json['comuna'],
    cutLocalidad : json['cut_localidad'],
    localidad : json['localidad'],
    calle : json['calle'],
    numero : json['numero'],
    depto : json['depto'],
    otro : json['otro'],
    gpsx : json['gpsx'],
    gpsy : json['gpsy'],
    telefono : json['telefono'],
    email : json['email'],
    sitioWeb : json['sitio_web'],
    idClase : json['id_clase'],
    wifi : json['wifi'],
    banoPrivado : json['bano_privado'],
    banoCompartido : json['bano_compartido'],
    bedAndBreakfast : json['bed_and_breakfast'],
    duchaCaliente : json['ducha_caliente'],
    tarjetaCredito : json['tarjeta_credito'],
    tarjetaDebito : json['tarjeta_debito'],
    cocinaCompartida : json['cocina_compartida'],
    livingRoom : json['living_room'],
    lavanderia : json['lavanderia'],
    accesoUniversal : json['acceso_universal'],
    areaAsadoFogon : json['area_asado_fogon'],
    menuEspecial : json['menu_especial'],
    productosLocales : json['productos_locales'],
    cervezaArtesanal : json['cerveza_artesanal'],
    transporteActividad : json['transporte_actividad'],
    equipamientoIncluido : json['equipamiento_incluido'],
    equipamientoArriendo : json['equipamiento_arriendo'],
    snackIncluido : json['snack_incluido'],
    actividadGuiada : json['actividad_guiada'],
    livOtro : json['liv_otro'],
    livOtroTexto : json['liv_otro_texto'],
  );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['id_inscripcion'] = this.idInscripcion;
    data['id_tipo_servicio'] = this.idTipoServicio;
    data['nombres'] = this.nombres;
    data['apellido_paterno'] = this.apellidoPaterno;
    data['apellido_materno'] = this.apellidoMaterno;
    data['razon_social'] = this.razonSocial;
    data['nombre_comercial'] = this.nombreComercial;
    data['cut_region'] = this.cutRegion;
    data['cut_comuna'] = this.cutComuna;
    data['comuna'] = this.comuna;
    data['cut_localidad'] = this.cutLocalidad;
    data['localidad'] = this.localidad;
    data['calle'] = this.calle;
    data['numero'] = this.numero;
    data['depto'] = this.depto;
    data['otro'] = this.otro;
    data['gpsx'] = this.gpsx;
    data['gpsy'] = this.gpsy;
    data['telefono'] = this.telefono;
    data['email'] = this.email;
    data['sitio_web'] = this.sitioWeb;
    data['id_clase'] = this.idClase;
    data['wifi'] = this.wifi;
    data['bano_privado'] = this.banoPrivado;
    data['bano_compartido'] = this.banoCompartido;
    data['bed_and_breakfast'] = this.bedAndBreakfast;
    data['ducha_caliente'] = this.duchaCaliente;
    data['tarjeta_credito'] = this.tarjetaCredito;
    data['tarjeta_debito'] = this.tarjetaDebito;
    data['cocina_compartida'] = this.cocinaCompartida;
    data['living_room'] = this.livingRoom;
    data['lavanderia'] = this.lavanderia;
    data['acceso_universal'] = this.accesoUniversal;
    data['area_asado_fogon'] = this.areaAsadoFogon;
    data['menu_especial'] = this.menuEspecial;
    data['productos_locales'] = this.productosLocales;
    data['cerveza_artesanal'] = this.cervezaArtesanal;
    data['transporte_actividad'] = this.transporteActividad;
    data['equipamiento_incluido'] = this.equipamientoIncluido;
    data['equipamiento_arriendo'] = this.equipamientoArriendo;
    data['snack_incluido'] = this.snackIncluido;
    data['actividad_guiada'] = this.actividadGuiada;
    data['liv_otro'] = this.livOtro;
    data['liv_otro_texto'] = this.livOtroTexto;
    return data;
  }
}
