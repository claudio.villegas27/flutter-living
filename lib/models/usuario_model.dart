class Usuario {
  int? id;
  int? id_usuario;
  String? name;
  String? email;
  String? rut;

  Usuario({
    this.id,
    this.id_usuario,
    this.name,
    this.email,
    this.rut,
  });
  factory Usuario.fromJson(Map<String, dynamic> json) => Usuario(
    id : json["id"],
    id_usuario : json["id_usuario"],
    name : json["name"],
    email : json["email"],
    rut : json["rut"],
  );

  Map<String, dynamic> toMap(){
    return {
      'id': id,
      'id_usuario': id_usuario,
      'name': name,
      'email': email,
      'rut': rut,
    };
  }
}
