

import 'dart:convert';
import 'dart:io';

import 'package:parque/models/usuario_model.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
class DBProvider{

  static Future<Database> _openDB() async {
    Directory documentDirectory = await getApplicationDocumentsDirectory();
    final path = join(await documentDirectory.path,'database.db');
    return openDatabase(path,
      onCreate: (db , version){
        return db.execute( "CREATE TABLE users(id INTEGER PRIMARY KEY AUTOINCREMENT, id_usuario INTEGER NOT NULL, name TEXT NOT NULL,email TEXT not null, rut TEXT NOT NULL)",
        );
      },
      version: 1);
  }
  static Future<int> insertUser(Usuario usuario) async {
        Database database = await _openDB();
        return database.insert("users", usuario.toMap());
  }
  static Future<int> deleteUser(Usuario usuario) async {
    Database database = await _openDB();

    return database.delete("users", where: "id = ?" , whereArgs: [usuario.id]);
  }
  static Future<Usuario?> getUser(id_usuario) async {
    Database database = await _openDB();

    final res = await database.query("users", where: "id_usuario = ?" , whereArgs: [id_usuario]);
    return res.isNotEmpty ? Usuario.fromJson(res.first) : null;

  }
  static Future<List<Usuario>?> getUsers() async {
    Database database = await _openDB();
    final res = await database.query("users");

    return res.isNotEmpty
        ? res.map( (json) => Usuario.fromJson(json)).toList()
        : null;
  }

}
