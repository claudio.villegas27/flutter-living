import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapGoogleProvider extends ChangeNotifier{

  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  late Set<Marker> _markers;

  set markers(Set<Marker> makers){
    _markers = makers;
    notifyListeners();
  }
  Set<Marker> get markers => _markers;

}
