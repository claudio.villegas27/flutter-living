import 'dart:typed_data';
import 'dart:ui' as ui;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:parque/models/models.dart';
import 'package:parque/screens/user/empresa_screen.dart';
import 'package:parque/servicios/auth_service.dart';
import 'package:provider/provider.dart';

class MapProvider extends ChangeNotifier{

  GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  Map<MarkerId, Marker> _markers_limpiar = <MarkerId, Marker>{};
  bool _activoLimpiar = false;
  BitmapDescriptor? icon_donde_comer, icon_donde_dormir, icon_que_hacer , icon_datos_utiles;
  bool _isChecked_que_hacer = true;
  bool _isChecked_donde_dormir = true;
  bool _isChecked_donde_comer = true;
  bool _isChecked_datos_utiles = true;
  //QUE HACER
  bool _hijos_isChecked_parques_trek = true;
  bool _hijos_isChecked_deporte_aventura = true;
  bool _hijos_isChecked_cultura = true;
  bool _hijos_otras_actividades = true;
  //DONDE COMER
  bool _hijos_isChecked_restaurante = true;
  bool _hijos_isChecked_cafe = true;
  bool _hijos_isChecked_comida_rapida = true;
  bool _hijos_isChecked_picada_cerveza_artesanal = true;
//DONDE DORMIR
  bool _hijos_isChecked_camping  = true;
  bool _hijos_isChecked_hotel  = true;
  bool _hijos_isChecked_cabania  = true;
  bool _hijos_isChecked_hostel  = true;
//DATOS UTILES
  bool _hijos_isChecked_bencineras = true;
  bool _hijos_isChecked_bancos = true;
  bool _hijos_isChecked_banios = true;
  bool _hijos_isChecked_farmacias = true;
  bool _hijos_isChecked_salud = true;
  bool _hijos_isChecked_ferreteria = true;
  bool _hijos_isChecked_policia = true;
  bool _hijos_isChecked_supermercado = true;
  bool _hijos_isChecked_wifi = true;


  bool _arrowQueHacerUp = true;
  bool _arrowDondeDormirUp = true;
  bool _arrowDondeComerUp = true;
  bool _arrowDatosUtiles = true;
  Set<Marker> _markeds = Set<Marker>();

  set markersLimpiar(Map<MarkerId, Marker> markers){
    _markers_limpiar = markers;
    notifyListeners();
  }
  Map<MarkerId, Marker> get markersLimpiar => _markers_limpiar;

  set markers(Set<Marker> makers){
    _markeds = makers;
    notifyListeners();
  }

  set activoLimpiar(bool estado) {
    _activoLimpiar = estado;
    notifyListeners();
  }

  bool get activoLimpiar => _activoLimpiar;


  //CATEGORIA PADRE
  set queHacer(bool estado) {
      _isChecked_que_hacer = estado;
    notifyListeners();
  }
  set dondeDormir(bool estado) {
    _isChecked_donde_dormir = estado;
    notifyListeners();
  }
  set dondeComer(bool estado) {
    _isChecked_donde_comer = estado;
    notifyListeners();
  }
  set datosUtiles(bool estado) {
    _isChecked_datos_utiles = estado;
    notifyListeners();
  }
  //QUE HACER
  set parquesTrek(bool estado) {
    _hijos_isChecked_parques_trek = estado;
    checkearQueHacer();
    notifyListeners();
  }
  set deportesAventura(bool estado) {
    _hijos_isChecked_deporte_aventura = estado;
    checkearQueHacer();
    notifyListeners();
  }
  set cultura(bool estado) {
    _hijos_isChecked_cultura = estado;
    checkearQueHacer();
    notifyListeners();
  }
  set otrasActividades(bool estado) {
    _hijos_otras_actividades = estado;
    checkearQueHacer();
    notifyListeners();
  }
//DONDE COMER
  set restaurante(bool estado) {
    _hijos_isChecked_restaurante = estado;
    checkearDondeComer();
    notifyListeners();
  }
  set cafe(bool estado) {
    _hijos_isChecked_cafe = estado;
    checkearDondeComer();
    notifyListeners();
  }
  set comidaRapida(bool estado) {
    _hijos_isChecked_comida_rapida = estado;
    checkearDondeComer();
    notifyListeners();
  }
  set cervezaArtesanal(bool estado) {
    _hijos_isChecked_picada_cerveza_artesanal = estado;
    checkearDondeComer();
    notifyListeners();
  }
//DONDE DORMIR
  set camping(bool estado) {
    _hijos_isChecked_camping = estado;
    checkearDondeDormir();
    notifyListeners();
  }
  set hotel(bool estado) {
    _hijos_isChecked_hotel = estado;
    checkearDondeDormir();
    notifyListeners();
  }
  set cabania(bool estado) {
    _hijos_isChecked_cabania = estado;
    checkearDondeDormir();
    notifyListeners();
  }
  set hostel(bool estado) {
    _hijos_isChecked_hostel = estado;
    checkearDondeDormir();
    notifyListeners();
  }
//DATOS UTILES
  set bencineras(bool estado) {
    _hijos_isChecked_bencineras = estado;
    checkearDatosUtiles();
    notifyListeners();
  }
  set bancos(bool estado) {
    _hijos_isChecked_bancos= estado;
    checkearDatosUtiles();
    notifyListeners();
  }
  set banios(bool estado) {
    _hijos_isChecked_banios = estado;
    checkearDatosUtiles();
    notifyListeners();
  }
  set farmacias(bool estado) {
    _hijos_isChecked_farmacias = estado;
    checkearDatosUtiles();
    notifyListeners();
  }
  set salud(bool estado) {
    _hijos_isChecked_salud = estado;
    checkearDatosUtiles();
    notifyListeners();
  }
  set ferreteria(bool estado) {
    _hijos_isChecked_ferreteria = estado;
    checkearDatosUtiles();
    notifyListeners();
  }
  set policia(bool estado) {
    _hijos_isChecked_policia = estado;
    checkearDatosUtiles();
    notifyListeners();
  }
  set supermercado(bool estado) {
    _hijos_isChecked_supermercado = estado;
    checkearDatosUtiles();
    notifyListeners();
  }
  set wifi(bool estado) {
    _hijos_isChecked_wifi = estado;
    checkearDatosUtiles();
    notifyListeners();
  }


  //CATEGORIA PADRE
  bool get queHacer => _isChecked_que_hacer;
  bool get dondeDormir => _isChecked_donde_dormir;
  bool get dondeComer => _isChecked_donde_comer;
  bool get datosUtiles => _isChecked_datos_utiles;
  //QUE HACER
  bool get parquesTrek => _hijos_isChecked_parques_trek;
  bool get deportesAventura => _hijos_isChecked_deporte_aventura;
  bool get cultura => _hijos_isChecked_cultura;
  bool get otrasActividades => _hijos_otras_actividades;
//DONDE COMER
  bool get restaurante => _hijos_isChecked_restaurante;
  bool get cafe => _hijos_isChecked_cafe;
  bool get comidaRapida => _hijos_isChecked_comida_rapida;
  bool get cervezaArtesanal => _hijos_isChecked_picada_cerveza_artesanal;
//DONDE DORMIR
  bool get camping => _hijos_isChecked_camping;
  bool get hotel => _hijos_isChecked_hotel;
  bool get cabania => _hijos_isChecked_cabania;
  bool get hostel => _hijos_isChecked_hostel;
//DATOS UTILES
  bool get bencineras => _hijos_isChecked_bencineras;
  bool get bancos => _hijos_isChecked_bancos;
  bool get banios => _hijos_isChecked_banios;
  bool get farmacias => _hijos_isChecked_farmacias;
  bool get salud => _hijos_isChecked_salud;
  bool get ferreteria => _hijos_isChecked_ferreteria;
  bool get policia => _hijos_isChecked_policia;
  bool get supermercado => _hijos_isChecked_supermercado;
  bool get wifi => _hijos_isChecked_wifi;





 //CATEGORIA PADRE
  String nombreQueHacer = 'QUE HACER ?';
  String nombreDondeDormir = 'DONDE DORMIR ?';
  String nombreDondeComer = 'DONDE COMER ?';
  String nombreDatosUtiles = 'DATOS UTILES ?';
//QUE HACER
  String nombreParqueTrek = 'PARQUES NACIONALES - TREKKING';
  String nombreDeporteAventura = 'DEPORTE Y AVENTURA';
  String nombreCultura = 'CULTURA?';
  String nombreOtrasActividades = 'OTRAS ACTIVIDADES ?';
//DONDE COMER
  String nombreRestaurante = 'RESTAURANTE ?';
  String nombreCafe = 'CAFE ?';
  String nombreComidaRapida = 'COMIDA RAPIDA ?';
  String nombrePicadaCerveza = 'PICADA - CERVEZA ?';
//DONDE DORMIR
  String nombreCamping = 'CAMPING ?';
  String nombreHotel = 'HOTEL ?';
  String nombreCabania = 'CABAÑA ?';
  String nombreHostel = 'HOSTEL ?';
//DATOS UTILES
  String nombreBencineras = 'BENCINERAS ?';
  String nombreBancos = 'BANCOS ?';
  String nombreBanios = 'BAÑIOS ?';
  String nombreFarmacias = 'FARMACIAS ?';
  String nombreSalud = 'CENTROS DE SALUD ?';
  String nombreFerreteria = 'FERRETERIA ?';
  String nombrePolicia = 'POLICIA ?';
  String nombreSupermercado = 'SUPERMERCADO ?';
  String nombreWifi = 'WIFI LIBRE ?';

//CATEGORIA-PADRE
  String  imgQueHacer = 'assets/que_hacer.png';
  String  imgDondeDormir = 'assets/donde_dormir.png';
  String  imgDondeComer = 'assets/donde_comer.png';
  String  imgDatosUtiles = 'assets/datos_utiles.png';
//QUE HACER
  String  imgParquesTrek = 'assets/que_hacer.png';
  String  imgCultura = 'assets/que_hacer.png';
  String  imgDeporteAventura = 'assets/que_hacer.png';
  String  imgOtrasActividades = 'assets/que_hacer.png';
// DONDE COMER
  String  imgRestaurante = 'assets/donde_comer.png';
  String  imgCafe = 'assets/donde_comer.png';
  String  imgComidaRapida = 'assets/donde_comer.png';
  String  imgPicadaCerveza = 'assets/donde_comer.png';
//DONDE DORMIR
  String  imgCamping = 'assets/donde_dormir.png';
  String  imgHotel = 'assets/donde_dormir.png';
  String  imgCabania = 'assets/donde_dormir.png';
  String  imgHostel = 'assets/donde_dormir.png';
//DATOS UTILES

  String  imgBencineras = 'assets/datos_utiles.png';
  String  imgBancos = 'assets/datos_utiles.png';
  String  imgBanios = 'assets/datos_utiles.png';
  String  imgFarmacias = 'assets/datos_utiles.png';
  String  imgSalud = 'assets/datos_utiles.png';
  String  imgFerreteria = 'assets/datos_utiles.png';
  String  imgPolicia = 'assets/datos_utiles.png';
  String  imgSupermercado = 'assets/datos_utiles.png';
  String  imgWifi = 'assets/datos_utiles.png';

    //---------------------ICONOS PARA FLECHAS----------------
  bool get arrowQueHacerUp => _arrowQueHacerUp;
  set arrowQueHacerUp(bool estado) {
    _arrowQueHacerUp = estado;
    checkearArrows(1);
    notifyListeners();
  }
  bool get arrowDondeDormirUp => _arrowDondeDormirUp;
  set arrowDondeDormirUp(bool estado) {
    _arrowDondeDormirUp = estado;
    checkearArrows(2);
    notifyListeners();
  }
  bool get arrowDondeComerUp => _arrowDondeComerUp;
  set arrowDondeComerUp(bool estado) {
    _arrowDondeComerUp = estado;
    checkearArrows(3);
    notifyListeners();
  }
  bool get arrowDatosUtiles => _arrowDatosUtiles;
  set arrowDatosUtiles(bool estado) {
    _arrowDatosUtiles = estado;
    checkearArrows(4);
    notifyListeners();
  }
//----------------FUNCIONES-------------------------------

  checkearArrows(int arrow){
    const arrow_queHacer = 1;
    const arrow_dondeDormir = 2;
    const arrow_dondeComer = 3;
    const arrow_datosUtiles = 4;
    switch(arrow){
      case arrow_queHacer:
        _arrowDondeDormirUp = true;
        _arrowDondeComerUp = true;
        _arrowDatosUtiles = true;
        break;
      case arrow_dondeDormir:
        _arrowQueHacerUp = true;
        _arrowDondeComerUp = true;
        _arrowDatosUtiles = true;
        break;
      case arrow_dondeComer:
        _arrowQueHacerUp = true;
        _arrowDondeDormirUp = true;
        _arrowDatosUtiles = true;
        break;
      case arrow_datosUtiles:
        _arrowQueHacerUp = true;
        _arrowDondeDormirUp = true;
        _arrowDondeComerUp = true;
        break;
    }
  }

  checkearQueHacer(){

    if(queHacer == false){
      _isChecked_que_hacer = true;
    }
    if(parquesTrek == false && deportesAventura == false && cultura == false && otrasActividades == false){
      _isChecked_que_hacer = true;
    }
  }
  checkearDondeDormir(){
    if(!dondeDormir){
      _isChecked_donde_dormir = true;
    }
    if(!camping && !hotel && !cabania && !hostel){
      _isChecked_donde_dormir = false;
    }
  }
  checkearDondeComer(){
    if(!dondeComer){
      _isChecked_donde_comer = true;
    }
    if(!restaurante && !cafe && !comidaRapida && !cervezaArtesanal){
      _isChecked_donde_comer = false;
    }
  }
  checkearDatosUtiles(){
    if(!datosUtiles){
      _isChecked_datos_utiles = true;
    }
    if(!bencineras && !bancos && !banios && !farmacias && !salud  && !ferreteria && !policia && !supermercado && !wifi ){
      _isChecked_datos_utiles = false;
    }
  }


  checkBoxQueHacer(){
    if(queHacer) {
      _hijos_isChecked_parques_trek = true;
      _hijos_isChecked_deporte_aventura = true;
      _hijos_isChecked_cultura = true;
      _hijos_otras_actividades = true;
    }else{
      _hijos_isChecked_parques_trek = false;
      _hijos_isChecked_deporte_aventura = false;
      _hijos_isChecked_cultura = false;
      _hijos_otras_actividades = false;
    }
  }

  checkBoxDondeDormir(){
    if(dondeDormir){
      _hijos_isChecked_camping = true;
      _hijos_isChecked_hotel = true;
      _hijos_isChecked_cabania = true;
      _hijos_isChecked_hostel = true;
    }else{
      _hijos_isChecked_camping = false;
      _hijos_isChecked_hotel = false;
      _hijos_isChecked_cabania  = false;
      _hijos_isChecked_hostel = false;
    }
  }

  checkBoxDondeComer(){
    if(dondeComer){
      _hijos_isChecked_restaurante = true;
      _hijos_isChecked_cafe = true;
      _hijos_isChecked_comida_rapida = true;
      _hijos_isChecked_picada_cerveza_artesanal = true;
    }else{
      _hijos_isChecked_restaurante = false;
      _hijos_isChecked_cafe = false;
      _hijos_isChecked_comida_rapida = false;
      _hijos_isChecked_picada_cerveza_artesanal = false;
    }
  }

  checkBoxDatosUtiles(){
    if(datosUtiles){
      _hijos_isChecked_bencineras = true;
       _hijos_isChecked_bancos = true;
      _hijos_isChecked_banios = true;
      _hijos_isChecked_farmacias = true;
      _hijos_isChecked_salud = true;
      _hijos_isChecked_ferreteria = true;
      _hijos_isChecked_policia = true;
      _hijos_isChecked_supermercado = true;
      _hijos_isChecked_wifi = true;
    }else{
      _hijos_isChecked_bencineras = false;
      _hijos_isChecked_bancos = false;
      _hijos_isChecked_banios = false;
      _hijos_isChecked_farmacias = false;
      _hijos_isChecked_salud = false;
      _hijos_isChecked_ferreteria = false;
      _hijos_isChecked_policia = false;
      _hijos_isChecked_supermercado = false;
      _hijos_isChecked_wifi = false;
    }
  }

  Set<Marker> get markers => _markeds;
  clearMarkeds(){

    _markeds.clear();
    notifyListeners();
  }
  Future<void> createMarkerImageFromAsset(BuildContext context , MapProvider mapProvider) async {
    int size = 50;
    if (icon_donde_dormir == null) {
      final Uint8List markerIcon = await getBytesFromAsset(mapProvider.imgDondeDormir, size);
      icon_donde_dormir = BitmapDescriptor.fromBytes(markerIcon);
    }
    if (icon_donde_comer == null) {
      final Uint8List markerIcon = await getBytesFromAsset(mapProvider.imgDondeComer, size);
      icon_donde_comer = BitmapDescriptor.fromBytes(markerIcon);
    }
    if (icon_que_hacer == null) {
      final Uint8List markerIcon = await getBytesFromAsset(mapProvider.imgQueHacer, size);
      icon_que_hacer = BitmapDescriptor.fromBytes(markerIcon);
    }
    if (icon_datos_utiles == null){
      final Uint8List markerIcon =  await getBytesFromAsset(mapProvider.imgDatosUtiles, size);
      icon_datos_utiles = BitmapDescriptor.fromBytes(markerIcon);
    }

  }
  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))!
        .buffer
        .asUint8List();
  }

  void removeMarkers(MapProvider mapProvider ,context ) {
    final authService = Provider.of<AuthService>(context, listen: false);
    final List<Servicios> listaServicios = authService.listaServicios;
    BitmapDescriptor markedIcon;
    Map<MarkerId, Marker> markers_limpiar = <MarkerId, Marker>{};
    if(mapProvider.queHacer && mapProvider.dondeDormir && mapProvider.dondeComer && mapProvider.datosUtiles){
      activoLimpiar = false;
    }else {
      markers_limpiar.clear();
      listaServicios.forEach((element) {
        double latitud = double.parse(element.gpsy!);
        double longitud = double.parse(element.gpsx!);
        if (mapProvider.queHacer) {
          switch (element.idTipoServicio) {
            case 1:
            case 3:
            case 4:
            case 5:
            case 9:
            case 10:
            case 11:
            case 12:
              markedIcon = icon_que_hacer!;

              final MarkerId markerId = MarkerId(element.id.toString());
              final Marker marker = Marker(
                  markerId: markerId,
                  position: LatLng(longitud, latitud),
                  icon: markedIcon,
                  visible: true,
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              AnuncioEmpresa(servicio: element)),
                    );
                  });
                markers_limpiar[markerId] = marker;
          }
        }
        if (mapProvider.dondeDormir) {

          switch(element.idTipoServicio) {
            case 2:
              markedIcon = icon_donde_dormir!;
              final MarkerId markerId = MarkerId(element.id.toString());
              final Marker marker = Marker(
                  markerId: markerId,
                  position: LatLng(longitud, latitud),
                  icon: markedIcon,
                  visible: true,
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              AnuncioEmpresa(servicio: element)),
                    );
                  });
                markers_limpiar[markerId] = marker;
              break;
          }
        }
        if (mapProvider.dondeComer) {
          switch(element.idTipoServicio) {
            case 6:
              markedIcon = icon_donde_comer!;
              final MarkerId markerId = MarkerId(element.id.toString());
              final Marker marker = Marker(
                  markerId: markerId,
                  position: LatLng(longitud, latitud),
                  icon: markedIcon,
                  visible: true,
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              AnuncioEmpresa(servicio: element)),
                    );
                  });
                markers_limpiar[markerId] = marker;
              break;
          }
        }
      });
      markersLimpiar = markers_limpiar;
      activoLimpiar = true;
    }
  }

















}
