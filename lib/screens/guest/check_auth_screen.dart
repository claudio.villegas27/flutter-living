import 'package:flutter/material.dart';
import 'package:parque/screens/guest/auth.dart';
import 'package:parque/screens/user/user.dart';
import 'package:parque/servicios/auth_service.dart';
import 'package:provider/provider.dart';

class CheckAuthScreen extends StatelessWidget {
  const CheckAuthScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final authService = Provider.of<AuthService>(context, listen: false);
    return Scaffold(
        body: Center(
            child: FutureBuilder(
      future: Future.wait([
        authService.esInvitado(),
      ]),
      builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
        if (snapshot.hasData) {
          return FutureBuilder(
            future: snapshot.data![0] == 'true'
                ? Future.wait([authService.obtenerServicios()])
                : Future.wait([
                    authService.isLogged(),
                    authService.obtenerServicios(),
                    //authService.getPlanes(),
                  ]),
            builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
              print(snapshot.data);
              if (snapshot.hasData) {
                if (snapshot.data == '' || snapshot.data?[0] == '') {
                  Future.microtask(() => {
                        Navigator.pushReplacement(
                            context,
                            PageRouteBuilder(
                              pageBuilder: (_, __, ___) => HomeScreen(),
                              transitionDuration: Duration(seconds: 1),
                            )),
                      });
                } else {
                  Future.microtask(() => {
                        Navigator.pushReplacement(
                            context,
                            PageRouteBuilder(
                              pageBuilder: (_, __, ___) => UserHomeScreen(),
                              transitionDuration: Duration(seconds: 1),
                            )),
                      });
                }
              }
              return Container();
            },
          );
        } 
        return Container();
      },
    )));
  }
}
