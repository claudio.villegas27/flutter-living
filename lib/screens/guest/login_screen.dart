import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:parque/providers/login_form_provider.dart';
import 'package:parque/servicios/servicios.dart';
import 'package:parque/ui/input_decorations.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    final TextTheme textTheme = Theme.of(context).textTheme;
    final size = MediaQuery.of(context).size;
    return SafeArea(
        child: Scaffold(
            body:LoaderOverlay(
              useDefaultLoading: false,
              overlayWidget: Center(
                child: SpinKitCubeGrid(
                  color: Colors.red,
                  size: 50.0,),),
              overlayOpacity: 0.8,
              child:SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(height: size.height * 0.2,),
                    Container(
                      height: size.height * 0.15,
                      width: size.width * 0.4,
                      child: FadeInImage(placeholder: AssetImage('assets/logo.png') , image: AssetImage('assets/logo.png'),
                        fit: BoxFit.fill,
                      ),
                    ),
                    SizedBox(height: size.height * 0.05),
                    ChangeNotifierProvider(
                      create: (_) => LoginFormProvider(),
                      child: _LoginForm(),

                    ),
                  ],
                ),
              ),
            )
        )
    );
  }
}

class _LoginForm extends StatefulWidget {

  @override
  __LoginFormState createState() => __LoginFormState();
}

class __LoginFormState extends State<_LoginForm> {

  bool _isLoaderVisible = false;
  @override
  Widget build(BuildContext context) {
    final loginProvider = Provider.of<LoginFormProvider>(context);

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 60),
      child: Form(
        key: loginProvider.formKey,
        //autovalidateMode: AutovalidateMode.onUserInteraction,
          child: Column(
            children: [
              TextFormField(
                autocorrect: false,
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecorations.authInputDecorariton(hintText: "Correo", labelText: "Ingrese Email"),
                onChanged: (value) => loginProvider.email = value,
                validator: (value) {
                  String pattern = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[-1-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                  RegExp regExp  = new RegExp(pattern);
                  return regExp.hasMatch(value ?? '')  ? null : "El Correo no es correcto" ;
                },
              ),

              SizedBox(height: 20,),
              TextFormField(
                autocorrect: false,
                obscureText: true,
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecorations.authInputDecorariton(hintText: "************", labelText: "Contraseña"),
                onChanged: (value) => loginProvider.password = value,
                validator: (value) {
                  if(value != null && value.length >= 6) return null; else return "Contraseña debe ser de 6 caracteres";
                },
              ),

              SizedBox(height: 20),
              Container(
                width: double.infinity,
                margin: EdgeInsets.only(top: 5),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 20)),
                  onPressed: loginProvider.isLoading ? null : () async {
                    final authService = Provider.of<AuthService>(context , listen: false);
                    FocusScope.of(context).unfocus();


                    if(loginProvider.isValidForm()){
                      context.loaderOverlay.show();
                      loginProvider.isLoading = true;
                      setState(() {
                        _isLoaderVisible = context.loaderOverlay.visible;
                      });

                      final Map<String,dynamic>? respuesta = await authService.loginUsuario(loginProvider.email, loginProvider.password);

                      if (_isLoaderVisible) {
                        context.loaderOverlay.hide();
                      }
                      setState(() {
                        _isLoaderVisible = context.loaderOverlay.visible;
                      });
                      loginProvider.isLoading = false;
                      if(respuesta!.containsKey('data')){

                        Navigator.of(context).pushNamedAndRemoveUntil('user_home', (Route<dynamic> route) => false);
                        // TODO VER PORQUE EL SNACKBAR NO SE LEVANTA
                        // NotificacionesService.showSnackBar("REGISTRO CREADO CON EXITO");
                      }else{
                        //TODO VER COMO MOSTRAR MENSAJES LLEGADO DE LARAVEL
                        print(respuesta);
                      }

                    }else{
                      print("FALSO");
                    }

                  },
                  child: Text(
                      loginProvider.isLoading ? 'Cargando' : "Ingresar",
                  ),
                ),
              )
            ],
          )),
    );
  }
}



