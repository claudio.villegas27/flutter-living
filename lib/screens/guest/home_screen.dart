import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:parque/screens/guest/auth.dart';
import 'package:parque/servicios/auth_service.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final TextTheme textTheme = Theme.of(context).textTheme;
    final size = MediaQuery.of(context).size;
    final authService = Provider.of<AuthService>(context , listen: false);
    final ButtonStyle style =
    ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 20));

    return SafeArea(
      child: Center(
        child: Scaffold(
          body: Stack(
            children:[
              Column(
              crossAxisAlignment: CrossAxisAlignment.center,

              children:[
                //IMG
                SizedBox(height: size.height * 0.2),
                Container(
                  height: size.height * 0.2,
                  width: size.width * 0.4,
                  child: FadeInImage(placeholder: AssetImage('assets/logo.png') , image: AssetImage('assets/logo.png'),
                    fit: BoxFit.contain,
                  ),
                ),
                SizedBox(height: size.height * 0.05),
                //TEXTO
                Padding(
                      padding: const EdgeInsets.all(7.0),
                      child: Text(
                        "BIENVENDIDO A LIVINGAPP , ENTERATE DE LAS NUEVAS NOVEDADES",
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.black , fontSize: size.width * 0.06),

                      ),
                    ),
                    //BTN CREAR CUENTA
                    //_btnCrearCuenta(size: size, style: style),
                    _invitado(size: size, style: style , authService: authService),
                  ]),

              _LinkIniciarSesion(size: size, textTheme: textTheme,),
            ]),
        ),
      ),
      );
  }
}
class _invitado extends StatelessWidget {

  const _invitado({
    Key? key,
    required this.size,
    required this.style,
    required this.authService,
  }) : super(key: key);
  final AuthService authService;
  final Size size;
  final ButtonStyle style;
  Widget build(BuildContext context) {

    return Container(
      width: size.width * 0.7,
      margin: EdgeInsets.only(top: 5),
      child: ElevatedButton(
        style: style,
        onPressed: () {
      authService.crearInvitado();
      Navigator.pushNamed(context, 'checkAuth');

        },
        child: const Text('INICIAR COMO INVITADO'),
      ),
    );
  }
}
class _btnCrearCuenta extends StatelessWidget {

  const _btnCrearCuenta({
    Key? key,
    required this.size,
    required this.style,
  }) : super(key: key);

  final Size size;
  final ButtonStyle style;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: size.width * 0.7,
      margin: EdgeInsets.only(top: 5),
      child: ElevatedButton(
        style: style,
        onPressed: () => Navigator.pushNamed(context, 'registro'),
        child: const Text('Crear una cuenta'),
      ),
    );
  }
}


class _LinkIniciarSesion extends StatelessWidget {


  final Size size;
  final TextTheme textTheme;

  const _LinkIniciarSesion({Key? key, required this.size, required this.textTheme}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Align(
          alignment: FractionalOffset.bottomCenter,
          child: Padding(
            padding: EdgeInsets.only(bottom: 30.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                new RichText(
                  text: new TextSpan(
                    children: [
                      new TextSpan(
                        text: "¿Ya tienes una cuenta? ",
                        style: textTheme.subtitle2,
                      ),
                      new TextSpan(
                        text: "Iniciar sesion",
                        style: new TextStyle(color: Colors.blueAccent),
                        recognizer: new TapGestureRecognizer()
                          ..onTap = () => Navigator.pushNamed(context, 'login') ,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
    );
  }
}

