import 'package:flutter/material.dart';
import 'package:parque/providers/register_form_provider.dart';
import 'package:parque/servicios/auth_service.dart';
import 'package:parque/servicios/servicios.dart';
import 'package:parque/ui/input_decorations.dart';
import 'package:provider/provider.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class RegisterScreen extends StatelessWidget {
  const RegisterScreen({Key? key}) : super(key: key);




  @override
  Widget build(BuildContext context) {

    final size = MediaQuery.of(context).size;
    return SafeArea(
        child: Scaffold(
          body: LoaderOverlay(
              useDefaultLoading: false,
              overlayWidget: Center(
                child: SpinKitCubeGrid(
                  color: Colors.red,
                  size: 50.0,
                ),
              ),
              overlayOpacity: 0.8,
            child:  SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: size.height * 0.1),
                  Container(
                    height: size.height * 0.2,
                    width: size.width * 0.4,
                    child: FadeInImage(placeholder: AssetImage('assets/logo.png') , image: AssetImage('assets/logo.png'),
                      fit: BoxFit.fill,
                    ),
                  ),
                  SizedBox(height: size.height * 0.05 , width: double.infinity,),
                  Text("Registro" , style: TextStyle(fontWeight: FontWeight.bold , fontSize: 35),),
                  ChangeNotifierProvider(
                    create: (_) => RegisterFormProvider(),
                    child:  _RegisterForm(),
                  ),

                ],
              ),
            ),
          ),
        )
    );
  }
}

class _RegisterForm extends StatefulWidget {




  const _RegisterForm({
    Key? key,
  }) : super(key: key);

  @override
  __RegisterFormState createState() => __RegisterFormState();
}

class __RegisterFormState extends State<_RegisterForm> {


  bool _isLoaderVisible = false;
  @override

  Widget build(BuildContext context) {


    final registerProvider = Provider.of<RegisterFormProvider>(context);
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 60),
      child: Form(
          key: registerProvider.formKey,
          child: Column(
            children: [
              TextFormField(
                autocorrect: false,
                keyboardType: TextInputType.text,
                decoration: InputDecorations.authInputDecorariton(hintText: "12.345.678-9", labelText: "Rut"),
                onChanged: (value) => registerProvider.rut = value,
                validator: (value) => value ==  '' ? "El Rut es obligatorio" : null,
                  //return regExp.hasMatch(value ?? '')  ? null : "El Rut no es correcto" ;
              ),
              TextFormField(
                autocorrect: false,
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecorations.authInputDecorariton(hintText: "parqueAustral@parque.cl", labelText: "Correo Electronico"),
                onChanged: (value) => registerProvider.email = value,
                validator: (value) {
                  String pattern = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[-1-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                  RegExp regExp  = new RegExp(pattern);
                  return regExp.hasMatch(value ?? '')  ? null : "El Correo no es correcto" ;
                },
              ),

              TextFormField(
                autocorrect: false,
                obscureText: true,
                keyboardType: TextInputType.emailAddress,
            decoration: InputDecorations.authInputDecorariton(hintText: "************", labelText:  "Contraseña"),
            onChanged: (value) => registerProvider.password = value,
            validator: (value) {
              if(value != null && value.length >= 6) return null; else return "Contraseña debe ser de 6 caracteres";
            },
          ),
          TextFormField(
            autocorrect: false,
            obscureText: true,
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecorations.authInputDecorariton(hintText: "************", labelText:  "Confirmar Contraseña"),
            onChanged: (value) =>  registerProvider.password_2 = value,
            validator: (value) {
              if(registerProvider.password  != registerProvider.password_2) return "La Contraseña no es igual"; else null;
              if(value != null && value.length >= 6) return null; else return "Contraseña debe ser de 6 caracteres";
            },
          ),
          SizedBox(height: 20),
          Container(
              width: double.infinity,
              margin: EdgeInsets.only(top: 5 ),
              child: ElevatedButton(
                  style: ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 20)),
                onPressed: registerProvider.isLoading ? null : () async {
                  final authService = Provider.of<AuthService>(context , listen: false);

                  try {


                  if(registerProvider.isValidForm()){
                    context.loaderOverlay.show();
                    registerProvider.isLoading = true;
                    setState(() {
                      _isLoaderVisible = context.loaderOverlay.visible;
                    });

                    //await Future.delayed(Duration(seconds: 2));
                    final Map<String,dynamic>? respuesta = await authService.crearUsuario(registerProvider.rut, registerProvider.email, registerProvider.password);
                    if(respuesta!.containsKey('data')){
                        print("Registro creado con exito");
                        Navigator.pushNamed(context, 'login');
                      // TODO VER PORQUE EL SNACKBAR NO SE LEVANTA
                      // NotificacionesService.showSnackBar("REGISTRO CREADO CON EXITO");
                    }else{
                      //TODO VER COMO MOSTRAR MENSAJES LLEGADO DE LARAVEL
                      print(respuesta);
                    }

                    if (_isLoaderVisible) {
                      context.loaderOverlay.hide();
                    }
                    setState(() {
                      _isLoaderVisible = context.loaderOverlay.visible;
                    });

                    registerProvider.isLoading = false;


                  }else{
                    print("FALSO");
                  }
                  } on Exception catch (exception) {

                  } catch (error) {

                    print(error);
                  }
                },
                child: const Text('Registrar'),
              ),
          )
        ],
          )),
    );
  }
}


