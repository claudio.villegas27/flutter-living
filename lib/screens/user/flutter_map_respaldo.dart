import 'dart:async';
import 'dart:typed_data';
import 'dart:ui' as ui;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:parque/models/models.dart';
import 'package:parque/providers/map_provider.dart';
import 'package:parque/servicios/auth_service.dart';
import 'package:parque/servicios/living_service.dart';
import 'package:parque/widgets/widgets.dart';
import 'package:provider/provider.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';


import 'empresa_screen.dart';

class UserHomeScreen extends StatelessWidget {
  const UserHomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return NavbarWidget(
      textAppbar: "Living App",
      body: ChangeNotifierProvider(
        create: (_) => MapProvider(),
        child: _UserHomeScreen(),
      ),
    );
  }
}

class _UserHomeScreen extends StatefulWidget {
  @override
  __UserHomeScreenState createState() => __UserHomeScreenState();
}

const LatLng _kMapCenter = LatLng(-45.5791519, -72.0608637);

class __UserHomeScreenState extends State<_UserHomeScreen> {
  GoogleMapController? mapController;
  BitmapDescriptor? icon_donde_comer, icon_donde_dormir, icon_que_hacer;
  var marker = <Marker>[];
  static final CameraPosition _kGooglePlex = CameraPosition(
    target: _kMapCenter,
    zoom: 14.4746,
  );

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final authService = Provider.of<AuthService>(context, listen: false);
    final List<Servicios> listaServicios = authService.listaServicios;
    final mapProvider = Provider.of<MapProvider>(context);

    Color getColor(Set<MaterialState> states) {
      const Set<MaterialState> interactiveStates = <MaterialState>{
        MaterialState.pressed,
        MaterialState.hovered,
        MaterialState.focused,
      };
      if (states.any(interactiveStates.contains)) {
        return Colors.blue;
      }
      return Colors.red;
    }

    _createMarkerImageFromAsset(context);
    //final size = MediaQuery.of(context).size;

    return Stack(children: [
      SizedBox(
        child: GoogleMap(
          initialCameraPosition: _kGooglePlex,
          markers: Set<Marker>.of(_createMarker(listaServicios)),
          onMapCreated: _onMapCreated,
          mapToolbarEnabled: false,
          zoomControlsEnabled: false,
        ),
      ),
      checkBox(mapProvider, getColor)
    ]);
  }

  void _onMapCreated(GoogleMapController controller) async {
    controller.setMapStyle(Options.mapStyle);
    mapController = controller;
  }

  Widget checkBox(MapProvider mapProvider, getColor) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Image.asset(
              'assets/que_hacer.png',
              width: 30.0,
            ),
            Checkbox(
              checkColor: Colors.white,
              fillColor: MaterialStateProperty.resolveWith(getColor),
              value: mapProvider.queHacer,
              onChanged: (bool? value) {
                mapProvider.queHacer = value!;

              },
            ),
          ],
        ),
        Row(mainAxisAlignment: MainAxisAlignment.end, children: [
          Image.asset(
            'assets/donde_dormir.png',
            width: 30.0,
          ),
          Checkbox(
              checkColor: Colors.white,
              fillColor: MaterialStateProperty.resolveWith(getColor),
              value: mapProvider.dondeDormir,
              onChanged: (bool? value) {
                mapProvider.dondeDormir = value!;
              }),
        ]),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Image.asset(
              'assets/donde_comer.png',
              width: 30.0,
            ),
            Checkbox(
              checkColor: Colors.white,
              fillColor: MaterialStateProperty.resolveWith(getColor),
              value: mapProvider.dondeComer,
              onChanged: (bool? value) {
                mapProvider.dondeComer = value!;
              },
            ),
          ],
        ),
      ],
    );
  }

  Future<void> _createMarkerImageFromAsset(BuildContext context) async {
    int size = 50;
    if (icon_donde_dormir == null) {
      final Uint8List markerIcon =
      await getBytesFromAsset('assets/donde_dormir.png', size);
      return _updateBitmap(
          BitmapDescriptor.fromBytes(markerIcon), "icon_donde_dormir");
    }
    if (icon_donde_comer == null) {
      final Uint8List markerIcon =
      await getBytesFromAsset('assets/donde_comer.png', size);
      return _updateBitmap(
          BitmapDescriptor.fromBytes(markerIcon), "icon_donde_comer");
    }
    if (icon_que_hacer == null) {
      final Uint8List markerIcon =
      await getBytesFromAsset('assets/que_hacer.png', size);
      return _updateBitmap(
          BitmapDescriptor.fromBytes(markerIcon), "icon_que_hacer");
    }
  }

  void _updateBitmap(BitmapDescriptor bitmap, String marker_string) {
    setState(() {
      if (marker_string == 'icon_que_hacer') icon_que_hacer = bitmap;
      if (marker_string == 'icon_donde_comer') icon_donde_comer = bitmap;
      if (marker_string == 'icon_donde_dormir') icon_donde_dormir = bitmap;
    });
  }

  List<Marker> _createMarker(List<Servicios> listaServicios) {
    var mar = <Marker>[];
    BitmapDescriptor markedIcon;
    listaServicios.forEach((element) {
      double latitud = double.parse(element.gpsy!);
      double longitud = double.parse(element.gpsx!);
      String id = element.id!.toString();

      switch (element.idTipoServicio) {
        case 1:
        case 3:
        case 4:
        case 5:
        case 9:
        case 10:
        case 11:
        case 12:
          markedIcon = icon_que_hacer!;
          break;
        case 2:
          markedIcon = icon_donde_dormir!;
          break;
        case 6:
          markedIcon = icon_donde_comer!;
          break;
        default:
          markedIcon = icon_que_hacer!;
      }

      mar.add(Marker(
          markerId: MarkerId(element.id.toString()),
          position: LatLng(longitud, latitud),
          icon: markedIcon,
          visible: true,
          onTap: () {
            print(element);
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => AnuncioEmpresa(servicio: element)),
            );
          }));
    });
    return mar;
  }
}

Future<Uint8List> getBytesFromAsset(String path, int width) async {
  ByteData data = await rootBundle.load(path);
  ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
      targetWidth: width);
  ui.FrameInfo fi = await codec.getNextFrame();
  return (await fi.image.toByteData(format: ui.ImageByteFormat.png))!
      .buffer
      .asUint8List();
}

class Options {
  static String mapStyle = ''' 
  [
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#212121"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#212121"
      }
    ]
  },
  {
    "featureType": "administrative",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "administrative.country",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "administrative.locality",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#bdbdbd"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#181818"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#1b1b1b"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#2c2c2c"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#8a8a8a"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#373737"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#3c3c3c"
      }
    ]
  },
  {
    "featureType": "road.highway.controlled_access",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#4e4e4e"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "transit",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#000000"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#3d3d3d"
      }
    ]
  }
]
  ''';
}
