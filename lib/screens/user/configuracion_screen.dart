import 'package:flutter/material.dart';
import 'package:parque/models/usuario_model.dart';
import 'package:parque/providers/db_provider.dart';
import 'package:parque/servicios/auth_service.dart';
import 'package:parque/ui/input_decorations.dart';
import 'package:parque/widgets/widgets.dart';
import 'package:provider/provider.dart';


class ConfiguracionScreen extends StatelessWidget {
  const ConfiguracionScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AuthService authService = Provider.of<AuthService>(context);
    return FutureBuilder(
        future: authService.obtenerUsuario(),
        builder: (BuildContext context, AsyncSnapshot<Usuario?> snapshot) {
          if (snapshot.hasData) {
            final usuario = snapshot.data!;
            return NavbarWidget(
                body: Container(
                  padding: EdgeInsets.symmetric(horizontal: 60),
                  child: Form(
                      child: Column(
                        children: [
                          TextFormField(
                            autocorrect: false,
                            keyboardType: TextInputType.text,
                            decoration: InputDecorations.authInputDecorariton(hintText: "Ingrese su nombre", labelText: "Nombre"),
                            validator: (value) => value ==  '' ? "El Nombre no puede ser vacio" : null,
                            initialValue: usuario.name,
                          ),


                          TextFormField(
                            autocorrect: false,
                            keyboardType: TextInputType.text,
                            decoration: InputDecorations.authInputDecorariton(hintText: "12.345.678-9", labelText: "Rut"),
                            validator: (value) => value ==  '' ? "El Rut es obligatorio" : null,
                            enabled: false,
                            initialValue: usuario.rut,
                          ),
                          TextFormField(
                            autocorrect: false,
                            keyboardType: TextInputType.emailAddress,
                            decoration: InputDecorations.authInputDecorariton(hintText: "aaaa@aaa.cl", labelText: "Correo Electronico"),
                            validator: (value) {
                              String pattern = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[-1-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                              RegExp regExp  = new RegExp(pattern);
                              return regExp.hasMatch(value ?? '')  ? null : "El Correo no es correcto" ;
                            },
                            initialValue: usuario.email,
                          ),

                          SizedBox(height: 20),
                          Container(
                            width: double.infinity,
                            margin: EdgeInsets.only(top: 5 ),
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 20)),
                              onPressed: () async {

                              },
                              child: const Text('Guardar'),
                            ),
                          )
                        ],
                      )),
                ),
                textAppbar: "CONFIGURACION"
            );
          } else if (snapshot.hasError) {
            return Center(
              child: Container(
                child:Text("OCURRIO UN ERROR PORFAVOR INTENTE NUEVAMENTE"),),
            );
            return Icon(Icons.error_outline);
          } else {
            return Center(child: CircularProgressIndicator());
          }
        });
  }
}
