import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:parque/models/models.dart';
import 'package:parque/screens/user/servicios_tab.dart';
import 'package:parque/widgets/widgets.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

import 'contacto_screen.dart';

//import 'httpDatosEmpresa.dart';
//import 'httpServicios.dart';
class AnuncioEmpresa extends StatelessWidget {


  final Servicios servicio;
  const AnuncioEmpresa({
    Key? key,
    required this.servicio,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return NavbarWidget(
      textAppbar: "Living App", body:Scaffold(
      // appBar: AppBar(
      //   // Here we take the value from the MyHomePage object that was created by
      //   // the App.build method, and use it to set our appbar title.
      //   title: Text(widget.title),
      // ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            miCardImageCarga(context),
            
            SizedBox(height: 20.0),
            
          ],
        ),
      ),

      floatingActionButtonLocation: FloatingActionButtonLocation.endTop,
      floatingActionButton: FloatingActionButton(
        tooltip: 'Contactarme',
        onPressed: () async {
          /*final String apiURL = ipServer + '/api/contactarme';
            final _dio = Dio();

            Response response = await _dio.post(apiURL,
                data: {'id_servicio': 1120, 'id_usuario': 20},
                options: Options(
                  receiveTimeout: 10000,
                ));

            print(response.data['respuesta']);
            if (response.data['respuesta']) {
              return _dialogBasicoSuccess(
                  context, "CONCTARME", "Su Solicitud Se Proceso Con Exito");
            } else {
              return _dialogBasicoError(
                  context, "Ups.", "Su solicitud no ah sido procesada");
            }


              */
        },
        child: Icon(Icons.contact_phone),
        backgroundColor: Colors.green,
      ),
      // floatingActionButton: FloatingActionButton(
      //   onPressed: _incrementCounter,
      //   tooltip: 'Increment',
      //   child: Icon(Icons.add),
      // ), // This trailing comma makes auto-formatting nicer for build methods.
    )
    );

  }
  Container tabPrecio() {
    return Container(
      child: Center(
        child: Text('PRECIO',
            style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold)),
      ),
    );
  }

  Container tabComentarios() {
    return Container(
      child: Center(
        child: Text('Comentarios',
            style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold)),
      ),
    );
  }

  Container tabServicios() {

    return Container(
      child: Column(
        children: [
          SizedBox(height: 10.0),
          Center(
              child: Text("SERVICIOS",
                  style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold))),
          Expanded(
            child: ServiciosTab(servicio: servicio,),
          ),
        ],
      ),
    );
  }


  _dialogBasicoSuccess(context, titulo, contenido) {
    Alert(
        context: context,
        title: titulo,
        desc: contenido,
        type: AlertType.success,
        buttons: [
          DialogButton(
            child: Text(
              "ACEPTAR",
              style: TextStyle(color: Colors.white, fontSize: 18),
            ),
            onPressed: () => Navigator.pop(context),
            gradient: LinearGradient(colors: [
              Color.fromRGBO(116, 116, 191, 1.0),
              Color.fromRGBO(52, 138, 199, 1.0)
            ]),
          )
        ]).show();
  }

  _dialogBasicoError(context, titulo, contenido) {
    Alert(
        context: context,
        title: titulo,
        desc: contenido,
        type: AlertType.error,
        buttons: [
          DialogButton(
            child: Text(
              "ACEPTAR",
              style: TextStyle(color: Colors.white, fontSize: 18),
            ),
            onPressed: () => Navigator.pop(context),
            gradient: LinearGradient(colors: [
              Color.fromRGBO(116, 116, 191, 1.0),
              Color.fromRGBO(52, 138, 199, 1.0)
            ]),
          )
        ]).show();
  }

   Card miCardImageCarga(context) {
    return Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
        margin: EdgeInsets.all(15),
        elevation: 10,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(30),
          child: Column(
            children: <Widget>[
              SizedBox(height: 20.0),
              CarouselWithIndicatorDemo( id_empresa: servicio.id!),
              Container(
                padding: EdgeInsets.all(10),
                child:Center(
              child: DefaultTabController(
                  length: 3, // length of tabs
                  initialIndex: 0,
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        Container(
                          child: TabBar(
                            labelColor: Colors.green,
                            unselectedLabelColor: Colors.black,
                            tabs: [
                              Tab(icon: Icon(Icons.message)),
                              Tab(
                                icon: Icon(Icons.sentiment_satisfied_alt),
                              ),
                              Tab(
                                icon: Icon(Icons.contact_page),
                              ),
                              //Tab(
                              //  icon: Icon(Icons.account_balance_wallet),
                             // ),
                            ],
                          ),
                        ),
                        Container(
                            height: MediaQuery.of(context).size.height *
                                0.55, //height of TabBarView
                            decoration: BoxDecoration(
                                border: Border(
                                    top: BorderSide(
                                        color: Colors.grey, width: 0.5))),
                            child: TabBarView(children: <Widget>[
                              ContactoTab(servicio: servicio),
                              tabServicios(),
                              tabComentarios(),
                              //tabPrecio()
                            ]))
                      ])),
            ),
              )
            ],
          ),
        ));
  }
}
