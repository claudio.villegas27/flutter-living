import 'dart:async';
import 'dart:typed_data';
import 'dart:ui' as ui;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:parque/models/models.dart';
import 'package:parque/providers/mapGoogleProvider.dart';
import 'package:parque/providers/map_provider.dart';
import 'package:parque/servicios/auth_service.dart';
import 'package:parque/servicios/living_service.dart';
import 'package:parque/widgets/widgets.dart';
import 'package:provider/provider.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:google_maps_flutter_platform_interface/src/types/marker_updates.dart';

import 'empresa_screen.dart';

class UserHomeScreen extends StatelessWidget {
  const UserHomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final authService = Provider.of<AuthService>(context);
    return FutureBuilder(
        future: Future.wait([
          authService.esInvitado(),
        ]),
        builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data![0] == 'true') {
              return NavbarWidget(
                textAppbar: "Perfil Invitado",
                body: _UserHomeScreen(),
              );
            } else {
              return NavbarWidget(
                textAppbar: "Living App",
                body: _UserHomeScreen(),
              );
            }
          }
          return Container();
        });
  }
}

class _UserHomeScreen extends StatefulWidget {
  @override
  __UserHomeScreenState createState() => __UserHomeScreenState();
}

typedef MarkerUpdateAction = Marker Function(Marker marker);

const LatLng _kMapCenter = LatLng(-45.5791519, -72.0608637);

class __UserHomeScreenState extends State<_UserHomeScreen> {
  __UserHomeScreenState();
  GoogleMapController? mapController;
  BitmapDescriptor? icon_donde_comer,
      icon_donde_dormir,
      icon_que_hacer,
      icon_datos_utiles;
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  Map<MarkerId, Marker> markers_limpiar = <MarkerId, Marker>{};
  static final CameraPosition _kGooglePlex = CameraPosition(
    target: _kMapCenter,
    zoom: 14.4746,
  );

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _onMapCreated(GoogleMapController controller) async {
    controller.setMapStyle(Options.mapStyle);
    mapController = controller;
  }

  Widget checkBox(MapProvider mapProvider, getColor, listaServicios) {
    final size = MediaQuery.of(context).size;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Container(
          width: size.width * 1,
            padding: EdgeInsets.only(
                left: size.width * 0.2, right: size.width * 0.2),
            child: Container(
             
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    GestureDetector(
                        onTap: () {
                          mapProvider.queHacer =
                              mapProvider.queHacer ? false : true;
                          print(mapProvider.queHacer);
                          mapProvider.checkBoxQueHacer();
                          mapProvider.removeMarkers(mapProvider, context);
                        },
                        child: CircleAvatar(
                          backgroundColor: Colors.white,
                          child: Image.asset(
                            mapProvider.imgQueHacer,
                            color: mapProvider.queHacer == false
                                ? Colors.white24
                                : null,
                          ),
                        )),
                    GestureDetector(
                        onTap: () {
                          mapProvider.dondeDormir =
                              mapProvider.dondeDormir ? false : true;
                          print(mapProvider.dondeDormir);
                          mapProvider.checkBoxDondeDormir();
                          mapProvider.removeMarkers(mapProvider, context);
                        },
                        child: CircleAvatar(
                          backgroundColor: Colors.white,
                          child: Image.asset(
                            mapProvider.imgDondeDormir,
                            color: mapProvider.dondeDormir == false
                                ? Colors.white24
                                : null,
                          ),
                        )),
                    GestureDetector(
                        onTap: () {
                          mapProvider.dondeComer =
                              mapProvider.dondeComer ? false : true;
                          print(mapProvider.dondeComer);
                          mapProvider.checkBoxDondeComer();
                          mapProvider.removeMarkers(mapProvider, context);
                        },
                        child: CircleAvatar(
                          backgroundColor: Colors.white,
                          child: Image.asset(
                            mapProvider.imgDondeComer,
                            color: mapProvider.dondeComer == false
                                ? Colors.white24
                                : null,
                          ),
                        )),
                    GestureDetector(
                        onTap: () {
                          mapProvider.datosUtiles =
                              mapProvider.datosUtiles ? false : true;
                          print(mapProvider.datosUtiles);
                          mapProvider.checkBoxDatosUtiles();
                          mapProvider.removeMarkers(mapProvider, context);
                        },
                        child: CircleAvatar(
                          backgroundColor: Colors.white,
                          child: Image.asset(
                            mapProvider.imgDatosUtiles,
                            color: mapProvider.datosUtiles == false
                                ? Colors.white24
                                : null,
                          ),
                        )),
                  ],
                ))),
        SizedBox(height: 20.0)
      ],
    );
  }

  Future<void> _createMarkerImageFromAsset(
      BuildContext context, MapProvider mapProvider) async {
    int size = 50;
    if (icon_donde_dormir == null) {
      final Uint8List markerIcon =
          await getBytesFromAsset(mapProvider.imgDondeDormir, size);
      return setState(
          () => icon_donde_dormir = BitmapDescriptor.fromBytes(markerIcon));
    }
    if (icon_donde_comer == null) {
      final Uint8List markerIcon =
          await getBytesFromAsset(mapProvider.imgDondeComer, size);
      return setState(
          () => icon_donde_comer = BitmapDescriptor.fromBytes(markerIcon));
    }
    if (icon_que_hacer == null) {
      final Uint8List markerIcon =
          await getBytesFromAsset(mapProvider.imgQueHacer, size);
      return setState(
          () => icon_que_hacer = BitmapDescriptor.fromBytes(markerIcon));
    }
    if (icon_datos_utiles == null) {
      final Uint8List markerIcon =
          await getBytesFromAsset(mapProvider.imgDatosUtiles, size);
      return setState(
          () => icon_datos_utiles = BitmapDescriptor.fromBytes(markerIcon));
    }
  }

  void _add(List<Servicios> listaServicios) {
    BitmapDescriptor markedIcon;
    listaServicios.forEach((element) {
      double latitud = double.parse(element.gpsy!);
      double longitud = double.parse(element.gpsx!);

      switch (element.idTipoServicio) {
        case 1:
        case 3:
        case 4:
        case 5:
        case 9:
        case 10:
        case 11:
        case 12:
          markedIcon = icon_que_hacer!;
          break;
        case 2:
          markedIcon = icon_donde_dormir!;
          break;
        case 6:
          markedIcon = icon_donde_comer!;
          break;
        // ignore: todo
        //TODO VER QUE TIPO DE ID ES EL DE DATOS UTILES
        default:
          markedIcon = icon_datos_utiles!;
      }
      final MarkerId markerId = MarkerId(element.id.toString());
      final Marker marker = Marker(
          markerId: markerId,
          position: LatLng(longitud, latitud),
          icon: markedIcon,
          visible: true,
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => AnuncioEmpresa(servicio: element)),
            );
          });
      setState(() {
        markers[markerId] = marker;
      });
    });
  }

  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))!
        .buffer
        .asUint8List();
  }

  @override
  Widget build(BuildContext context) {
    final authService = Provider.of<AuthService>(context, listen: false);
    final List<Servicios> listaServicios = authService.listaServicios;
    final mapProvider = Provider.of<MapProvider>(context);

    Color getColor(Set<MaterialState> states) {
      const Set<MaterialState> interactiveStates = <MaterialState>{
        MaterialState.pressed,
        MaterialState.hovered,
        MaterialState.focused,
      };
      if (states.any(interactiveStates.contains)) {
        return Colors.blue;
      }
      return Colors.red;
    }

    mapProvider.createMarkerImageFromAsset(context, mapProvider);
    _createMarkerImageFromAsset(context, mapProvider);
    _add(listaServicios);
    final size = MediaQuery.of(context).size;

    return Stack(children: [
      Column(
        children: [
          Expanded(
            child: GoogleMap(
              //mapType: MapType.hybrid,

              initialCameraPosition: _kGooglePlex,
              markers: Set<Marker>.of(mapProvider.activoLimpiar
                  ? mapProvider.markersLimpiar.values
                  : markers.values),
              onMapCreated: _onMapCreated,
              mapToolbarEnabled: false,
              zoomControlsEnabled: false,
            ),
          ),
        ],
      ),
      checkBox(mapProvider, getColor, listaServicios),
    ]);
  }
}

class Options {
  static String mapStyle = ''' 
  [
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#212121"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#212121"
      }
    ]
  },
  {
    "featureType": "administrative",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "administrative.country",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "administrative.locality",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#bdbdbd"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#181818"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#1b1b1b"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#2c2c2c"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#8a8a8a"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#373737"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#3c3c3c"
      }
    ]
  },
  {
    "featureType": "road.highway.controlled_access",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#4e4e4e"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "transit",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#000000"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#3d3d3d"
      }
    ]
  }
]
  ''';
}
