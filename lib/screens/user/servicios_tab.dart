import 'dart:async';

import 'package:flutter/material.dart';
import 'package:parque/models/models.dart';

class ServiciosTab extends StatelessWidget{
  final Servicios servicio;
  const ServiciosTab({
    Key? key,
    required this.servicio,
  }) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: Text('JSON ListView in Flutter'),
      // ),
      body: ListView(children: [
            listaServicios(servicio.wifi, 'WIFI', 'assets/02_icon_serv_wifi.png'),
            listaServicios(servicio.banoPrivado,'BAÑO PRIVADO', 'assets/01_icon_serv_bano.png'),
            listaServicios(servicio.banoCompartido,'BAÑO COMPARTIDO', 'assets/01_icon_serv_bano.png'),
            listaServicios(servicio.bedAndBreakfast,'BED AND BREAKFAST', 'assets/16_icon_serv_snack.png'),
            listaServicios(servicio.duchaCaliente,'DUCHA CALIENTE', 'assets/03_icon_serv_ducha.png'),
            listaServicios(servicio.tarjetaCredito,'TARJETA DE CREDITO', 'assets/14_icon_serv_credito.png'),
            listaServicios(servicio.tarjetaDebito,'TARJETA DE DEBITO', 'assets/13_icon_serv_debito.png'),
            listaServicios(servicio.cocinaCompartida,'COCINA COMPARTIDA', 'assets/07_icon_serv_cocina.png'),
            listaServicios(servicio.lavanderia, 'LAVANDERIA','assets/04_icon_serv_lavanderia.png'),
            listaServicios(servicio.accesoUniversal,'ACCESO UNIVERSAL', 'assets/08_icon_serv_accesibilidad.png'),
            listaServicios(servicio.areaAsadoFogon,'AREA ASADO FOGON', 'assets/12_icon_serv_fogon.png'),
            listaServicios(servicio.menuEspecial,'MENU ESPECIAL', 'assets/06_icon_serv_desayuno.png'),
            listaServicios(servicio.productosLocales,'PRODUCTOS LOCALES', 'assets/09_icon_serv_artesania.png'),
            listaServicios(servicio.cervezaArtesanal,'CERVEZA ARTESANAL', 'assets/15_icon_serv_cerveza.png'),
            listaServicios(servicio.transporteActividad,'TRANSPORTE ACTIVIDAD', 'assets/10_icon_serv_transporte.png'),
            listaServicios(servicio.equipamientoIncluido,'EQUIPAMIENTO INCLUIDO', 'assets/11_icon_serv_equipo.png'),
            listaServicios(servicio.equipamientoArriendo,'EQUIPAMIENTO ARRIENDO', 'assets/11_icon_serv_equipo.png'),
            listaServicios(servicio.snackIncluido,'SNACK INCLUIDO', 'assets/16_icon_serv_snack.png'),
            listaServicios(servicio.actividadGuiada,'ACTIVIDAD GUIADA', 'assets/16_icon_serv_snack.png'),
            listaServicios(servicio.livOtro, 'LIV OTRO','assets/16_icon_serv_snack.png'),
            listaServicios(servicio.livOtroTexto,'LIV OTRO TEXTO', 'assets/16_icon_serv_snack.png'),
          ])
    );
  }

  ListTile listaServicios(servicio, nombre, icon) {
    return ListTile(
        title: Text(nombre),
        onTap: () {},
        subtitle: (servicio == 1) ? Text("SI TIENE") : Text("NO TIENE"),
        leading: (servicio == 1)
            ? CircleAvatar(
          backgroundColor: Colors.white,
          child: Image.asset(
            icon,
            color: Colors.green,
          ),
        )
            : CircleAvatar(
          backgroundColor: Colors.white,
          child: Image.asset(
            icon,
            color: Colors.red,
          ),
        ));
  }
}
