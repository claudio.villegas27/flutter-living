import 'dart:async';
import 'package:flutter/material.dart';
import 'package:parque/models/models.dart';
import 'package:parque/servicios/servicios.dart';
import 'package:provider/provider.dart';

class HttpObtenerDatosEmpresa extends StatefulWidget {
  CustomJSONListView createState() => CustomJSONListView();
}

class CustomJSONListView extends State {


  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final authService = Provider.of<AuthService>(context , listen: false);

    return Scaffold(
      // appBar: AppBar(
      //   title: Text('JSON ListView in Flutter'),
      // ),
      body: FutureBuilder<Servicios?>(
        future: authService.obtenerInfoEmpresa(id),
        builder: (BuildContext context,
            AsyncSnapshot snapshot) {
          if (!snapshot.hasData)
            return Center(child: CircularProgressIndicator());
          if (snapshot.data['error'] == true) return Text("ERROR");
          return Card(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                          width: 300.0,
                          child: textBold(
                              snapshot.data['data']['nombre_comercial'], 22)),
                      Divider(),
                    ],
                  ),
                  Center(
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Column(
                        children: [
                          SizedBox(height: 40.0),
                          Wrap(
                              alignment: WrapAlignment.spaceBetween,
                              spacing: 50.0, // gap between adjacent chips
                              runSpacing: 10.0, // gap between lines
                              direction: Axis.horizontal,
                              children: <Widget>[
                                textBold('RUT EMPRESA :', 20.0),
                                textBold(
                                    snapshot.data['data']['rut_completo'], 20.0),
                                textBold('DIRECCION :', 20.0),
                                textBold(snapshot.data['data']['direccion'], 20.0),
                                textBold('EMAIL :', 20.0),
                                textBold(snapshot.data['data']['email'], 20.0),
                                textBold('TELEFONO :', 20.0),
                                textBold(snapshot.data['data']['numero1'], 20.0),
                                if (snapshot.data['data']['numero2'] != null)
                                  textBold('TELEFONO 2 :', 20.0),
                                if (snapshot.data['data']['numero2'] != null)
                                  textBold(snapshot.data['data']['numero2'], 20.0),
                                textBold('REGION :', 20.0),
                                textBold(snapshot.data['data']['region'], 20.0),
                                textBold('PROVINCIA :', 20.0),
                                textBold(snapshot.data['data']['provincia'], 20.0),
                              ]),
                          // Wrap(
                          //     spacing: 8.0, // gap between adjacent chips
                          //     runSpacing: 4.0, // gap between lines
                          //     direction: Axis.horizontal,
                          //     children: <Widget>[
                          //       Chip(
                          //         label: textBold(
                          //             snapshot.data['data']['rut_completo'], 15.0),
                          //       ),
                          //       Chip(
                          //         label: textBold(
                          //             snapshot.data['data']['direccion'], 15.0),
                          //         avatar: CircleAvatar(
                          //           backgroundImage: NetworkImage(
                          //               'https://pbs.twimg.com/profile_images/988272404915875840/lE7ZkrO-_400x400.jpg'),
                          //         ),
                          //       ),
                          //       Chip(
                          //         label: textBold(
                          //             snapshot.data['data']['region'], 15.0),
                          //       ),
                          //       Chip(
                          //         label: textBold(
                          //             snapshot.data['data']['region'], 15.0),
                          //       ),
                          //       Chip(
                          //         label: textBold(
                          //             snapshot.data['data']['region'], 15.0),
                          //       )
                          //     ]),

                          // Row(
                          //   mainAxisAlignment: MainAxisAlignment.center,
                          //   children: [
                          //     FlatButton.icon(
                          //         onPressed: () {},
                          //         icon: Icon(
                          //           Icons.quick_contacts_dialer,
                          //           size: 40.0,
                          //         ),
                          //         color: Colors.black54,
                          //         label: Text('Quiero que me Contacten')),
                          //   ],
                          // )
                        ],
                      ),
                    ),
                  )
                ],
              ));
        },
      ),
    );
  }

  Text textBold(datos, double font) {
    return Text(datos,
        style: TextStyle(fontSize: font, fontWeight: FontWeight.bold));
  }
}
