import 'package:flutter/material.dart';
import 'package:parque/models/models.dart';
import 'package:parque/servicios/auth_service.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class ContactoTab extends StatelessWidget {
  final Servicios servicio;
  const ContactoTab({
    Key? key,
    required this.servicio,
  }) : super(key: key);

 Future<void> _launchInBrowser(Uri url) async {
    if (!await launchUrl(
      url,
      mode: LaunchMode.externalApplication,
    )) {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    final authService = Provider.of<AuthService>(context, listen: false);
    final size = MediaQuery.of(context).size;
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        SizedBox(height: size.height * 0.025),
        header(servicio, size),
        body('Telefono' ,servicio.telefono.toString(), size),
        body('Correo Electronico' ,servicio.email.toString(), size),
        body('Sitio web' ,servicio.sitioWeb != '' ? servicio.sitioWeb.toString() : 'No tiene', size),
        body('Razon Social' ,servicio.razonSocial != '' ? servicio.razonSocial.toString(): 'No tiene', size),
      ],
    );
  }

  Widget header(Servicios servicio, size) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(children: [
          textPadding(
              servicio.nombreComercial.toString(), true, size.width * 0.05),
        ]),
        textPadding(
            servicio.localidad != ''
                ? servicio.localidad.toString()
                : 'LOCALIDAD',
            false,
            size.width * 0.035),
        textPadding(
            servicio.comuna != '' ? servicio.comuna.toString() : 'COMUNA',
            false,
            size.width * 0.035),
        Divider(),
      ],
    );
  }
}

Widget body( String titulo, String dato, size) {
  return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(children: [
          textPadding(titulo, true, size.width * 0.04),
        ]),
        Padding(
          padding: EdgeInsets.fromLTRB(5, 5, 0, 0),
          child: textPadding(
              dato, false, size.width * 0.04),
        ),
        Divider(),

        
      ]);
}

Widget textPadding(String texto, bool bold, double fontsize) {
  return Padding(
      padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
      child: Text(texto,
          style: TextStyle(
              fontSize: fontsize, fontWeight: bold ? FontWeight.bold : null)));
}
