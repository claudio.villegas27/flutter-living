import 'package:flutter/material.dart';

class InputDecorations{

  static InputDecoration authInputDecorariton({
    required String hintText,
    required String labelText,
    IconData? iconInput,
  }){
    return InputDecoration(
      enabledBorder: UnderlineInputBorder(
        borderSide: BorderSide(
            color: Colors.deepPurple
        ),

      ),
      focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color:Colors.blueAccent,
          )
      ),
      hintText: hintText,
      labelText: labelText,
      labelStyle: TextStyle(
        color: Colors.grey,
      ),
      prefixIcon: iconInput != null? Icon(iconInput , color: Colors.deepPurple,) : null ,
    );
  }
}