import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:parque/providers/map_provider.dart';
import 'package:parque/screens/guest/auth.dart';
import 'package:parque/screens/user/user.dart';
import 'package:parque/servicios/servicios.dart';
import 'package:provider/provider.dart';
import 'package:getwidget/getwidget.dart';
import 'package:accordion/accordion.dart';

class NavbarWidget extends StatelessWidget {
  final Widget body;
  final String textAppbar;
  final Widget? btnFloat;
  NavbarWidget({required this.body, required this.textAppbar, this.btnFloat});

  final GlobalKey<ScaffoldState> _keyScaffold = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    final mapProvider = Provider.of<MapProvider>(context);
    final size = MediaQuery.of(context).size;
    return Scaffold(
      key: _keyScaffold,
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: ListTile(
          title: Text(
            textAppbar,
            style: TextStyle(color: Colors.white),
          ),
        ),
        automaticallyImplyLeading: false,
        leading: IconButton(
            onPressed: () {
              _keyScaffold.currentState!.openDrawer();
            },
            icon: Icon(Icons.menu)),
        titleSpacing: size.width * 0.2,
      ),
      drawer: _drawer(context, mapProvider),
      body: body,
      floatingActionButton: btnFloat != null ? btnFloat : null,
    );
  }
}

Color getColor(Set<MaterialState> states) {
  const Set<MaterialState> interactiveStates = <MaterialState>{
    MaterialState.pressed,
    MaterialState.hovered,
    MaterialState.focused,
  };
  if (states.any(interactiveStates.contains)) {
    return Colors.blue;
  }
  return Colors.red;
}

Widget checkBoxQueHacer(MapProvider mapProvider, getColor, context) {
  return Column(
    children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Image.asset(
            mapProvider.imgQueHacer,
            width: 30.0,
          ),
          Checkbox(
              checkColor: Colors.white,
              fillColor: MaterialStateProperty.resolveWith(getColor),
              value: mapProvider.queHacer,
              onChanged: (bool? value) {
                mapProvider.queHacer = value!;
                mapProvider.checkBoxQueHacer();
                mapProvider.removeMarkers(mapProvider, context);
              }),
          Text(
            mapProvider.nombreQueHacer,
            style: TextStyle(color: Colors.white),
          ),
          Expanded(
            child: Align(
              alignment: Alignment.centerRight,
              child: IconButton(
                icon: mapProvider.arrowQueHacerUp == true
                    ? Icon(
                        Icons.keyboard_arrow_down_outlined,
                        color: Colors.white,
                      )
                    : Icon(
                        Icons.keyboard_arrow_up_outlined,
                        color: Colors.white,
                      ),
                highlightColor: Colors.white,
                onPressed: () =>
                    mapProvider.arrowQueHacerUp = !mapProvider.arrowQueHacerUp,
              ),
            ),
          ),
        ],
      ),
      mapProvider.arrowQueHacerUp == false
          ? Padding(
              padding: const EdgeInsets.only(left: 20.0),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Image.asset(
                        mapProvider.imgParquesTrek,
                        width: 30.0,
                      ),
                      Checkbox(
                          checkColor: Colors.white,
                          fillColor:
                              MaterialStateProperty.resolveWith(getColor),
                          value: mapProvider.parquesTrek,
                          onChanged: (bool? value) {
                            mapProvider.parquesTrek = value!;
                          }),
                      Text(
                        mapProvider.nombreParqueTrek,
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Image.asset(
                        mapProvider.imgDeporteAventura,
                        width: 30.0,
                      ),
                      Checkbox(
                          checkColor: Colors.white,
                          fillColor:
                              MaterialStateProperty.resolveWith(getColor),
                          value: mapProvider.deportesAventura,
                          onChanged: (bool? value) {
                            mapProvider.deportesAventura = value!;
                          }),
                      Text(
                        mapProvider.nombreDeporteAventura,
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Image.asset(
                        mapProvider.imgCultura,
                        width: 30.0,
                      ),
                      Checkbox(
                          checkColor: Colors.white,
                          fillColor:
                              MaterialStateProperty.resolveWith(getColor),
                          value: mapProvider.cultura,
                          onChanged: (bool? value) {
                            mapProvider.cultura = value!;
                          }),
                      Text(
                        mapProvider.nombreCultura,
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Image.asset(
                        mapProvider.imgOtrasActividades,
                        width: 30.0,
                      ),
                      Checkbox(
                          checkColor: Colors.white,
                          fillColor:
                              MaterialStateProperty.resolveWith(getColor),
                          value: mapProvider.otrasActividades,
                          onChanged: (bool? value) {
                            mapProvider.otrasActividades = value!;
                          }),
                      Text(
                        mapProvider.nombreOtrasActividades,
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                ],
              ),
            )
          : Center(),
    ],
  );
}

Widget checkBoxDondeComer(MapProvider mapProvider, getColor, context) {
  return Column(
    children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Image.asset(
            mapProvider.imgDondeComer,
            width: 30.0,
          ),
          Checkbox(
              checkColor: Colors.white,
              fillColor: MaterialStateProperty.resolveWith(getColor),
              value: mapProvider.dondeComer,
              onChanged: (bool? value) {
                mapProvider.dondeComer = value!;
                mapProvider.checkBoxDondeComer();
                mapProvider.removeMarkers(mapProvider, context);
              }),
          Text(
            mapProvider.nombreDondeComer,
            style: TextStyle(color: Colors.white),
          ),
          Expanded(
            child: Align(
              alignment: Alignment.centerRight,
              child: IconButton(
                icon: mapProvider.arrowDondeComerUp == true
                    ? Icon(
                        Icons.keyboard_arrow_down_outlined,
                        color: Colors.white,
                      )
                    : Icon(
                        Icons.keyboard_arrow_up_outlined,
                        color: Colors.white,
                      ),
                highlightColor: Colors.white,
                onPressed: () => mapProvider.arrowDondeComerUp =
                    !mapProvider.arrowDondeComerUp,
              ),
            ),
          ),
        ],
      ),
      mapProvider.arrowDondeComerUp == false
          ? Padding(
              padding: const EdgeInsets.only(left: 20.0),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Image.asset(
                        mapProvider.imgRestaurante,
                        width: 30.0,
                      ),
                      Checkbox(
                          checkColor: Colors.white,
                          fillColor:
                              MaterialStateProperty.resolveWith(getColor),
                          value: mapProvider.restaurante,
                          onChanged: (bool? value) {
                            mapProvider.restaurante = value!;
                          }),
                      Text(
                        mapProvider.nombreRestaurante,
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Image.asset(
                        mapProvider.imgCafe,
                        width: 30.0,
                      ),
                      Checkbox(
                          checkColor: Colors.white,
                          fillColor:
                              MaterialStateProperty.resolveWith(getColor),
                          value: mapProvider.cafe,
                          onChanged: (bool? value) {
                            mapProvider.cafe = value!;
                          }),
                      Text(
                        mapProvider.nombreCafe,
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Image.asset(
                        mapProvider.imgComidaRapida,
                        width: 30.0,
                      ),
                      Checkbox(
                          checkColor: Colors.white,
                          fillColor:
                              MaterialStateProperty.resolveWith(getColor),
                          value: mapProvider.comidaRapida,
                          onChanged: (bool? value) {
                            mapProvider.comidaRapida = value!;
                          }),
                      Text(
                        mapProvider.nombreComidaRapida,
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Image.asset(
                        mapProvider.imgPicadaCerveza,
                        width: 30.0,
                      ),
                      Checkbox(
                          checkColor: Colors.white,
                          fillColor:
                              MaterialStateProperty.resolveWith(getColor),
                          value: mapProvider.cervezaArtesanal,
                          onChanged: (bool? value) {
                            mapProvider.cervezaArtesanal = value!;
                          }),
                      Text(
                        mapProvider.nombrePicadaCerveza,
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                ],
              ),
            )
          : Center(),
    ],
  );
}

Widget checkBoxDondeDormir(MapProvider mapProvider, getColor, context) {
  return Column(
    children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Image.asset(
            mapProvider.imgDondeDormir,
            width: 30.0,
          ),
          Checkbox(
              checkColor: Colors.white,
              fillColor: MaterialStateProperty.resolveWith(getColor),
              value: mapProvider.dondeDormir,
              onChanged: (bool? value) {
                mapProvider.dondeDormir = value!;
                mapProvider.checkBoxDondeDormir();
                mapProvider.removeMarkers(mapProvider, context);
              }),
          Text(
            mapProvider.nombreDondeDormir,
            style: TextStyle(color: Colors.white),
          ),
          Expanded(
            child: Align(
              alignment: Alignment.centerRight,
              child: IconButton(
                icon: mapProvider.arrowDondeDormirUp == true
                    ? Icon(
                        Icons.keyboard_arrow_down_outlined,
                        color: Colors.white,
                      )
                    : Icon(
                        Icons.keyboard_arrow_up_outlined,
                        color: Colors.white,
                      ),
                highlightColor: Colors.white,
                onPressed: () => mapProvider.arrowDondeDormirUp =
                    !mapProvider.arrowDondeDormirUp,
              ),
            ),
          ),
        ],
      ),
      mapProvider.arrowDondeDormirUp == false
          ? Padding(
              padding: const EdgeInsets.only(left: 20.0),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Image.asset(
                        mapProvider.imgCamping,
                        width: 30.0,
                      ),
                      Checkbox(
                          checkColor: Colors.white,
                          fillColor:
                              MaterialStateProperty.resolveWith(getColor),
                          value: mapProvider.camping,
                          onChanged: (bool? value) {
                            mapProvider.camping = value!;
                          }),
                      Text(
                        mapProvider.nombreCamping,
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Image.asset(
                        mapProvider.imgHotel,
                        width: 30.0,
                      ),
                      Checkbox(
                          checkColor: Colors.white,
                          fillColor:
                              MaterialStateProperty.resolveWith(getColor),
                          value: mapProvider.hotel,
                          onChanged: (bool? value) {
                            mapProvider.hotel = value!;
                          }),
                      Text(
                        mapProvider.nombreHotel,
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Image.asset(
                        mapProvider.imgCabania,
                        width: 30.0,
                      ),
                      Checkbox(
                          checkColor: Colors.white,
                          fillColor:
                              MaterialStateProperty.resolveWith(getColor),
                          value: mapProvider.cabania,
                          onChanged: (bool? value) {
                            mapProvider.cabania = value!;
                          }),
                      Text(
                        mapProvider.nombreCabania,
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Image.asset(
                        mapProvider.imgHostel,
                        width: 30.0,
                      ),
                      Checkbox(
                          checkColor: Colors.white,
                          fillColor:
                              MaterialStateProperty.resolveWith(getColor),
                          value: mapProvider.hostel,
                          onChanged: (bool? value) {
                            mapProvider.hostel = value!;
                          }),
                      Text(
                        mapProvider.nombreHostel,
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                ],
              ),
            )
          : Center(),
    ],
  );
}

Widget checkBoxDatosUtiles(MapProvider mapProvider, getColor, context) {
  return Column(
    children: [
      Row(
        children: [
          Image.asset(
            mapProvider.imgDatosUtiles,
            width: 30.0,
          ),
          Checkbox(
              checkColor: Colors.white,
              fillColor: MaterialStateProperty.resolveWith(getColor),
              value: mapProvider.datosUtiles,
              onChanged: (bool? value) {
                mapProvider.datosUtiles = value!;
                mapProvider.checkBoxDatosUtiles();
                mapProvider.removeMarkers(mapProvider, context);
              }),
          Text(
            mapProvider.nombreDatosUtiles,
            style: TextStyle(color: Colors.white),
          ),
          Expanded(
            child: Align(
              alignment: Alignment.centerRight,
              child: IconButton(
                icon: mapProvider.arrowDatosUtiles == true
                    ? Icon(
                        Icons.keyboard_arrow_down_outlined,
                        color: Colors.white,
                      )
                    : Icon(
                        Icons.keyboard_arrow_up_outlined,
                        color: Colors.white,
                      ),
                highlightColor: Colors.white,
                onPressed: () => mapProvider.arrowDatosUtiles =
                    !mapProvider.arrowDatosUtiles,
              ),
            ),
          ),
        ],
      ),
      mapProvider.arrowDatosUtiles == false
          ? Padding(
              padding: const EdgeInsets.only(left: 20.0),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Image.asset(
                        mapProvider.imgBencineras,
                        width: 30.0,
                      ),
                      Checkbox(
                          checkColor: Colors.white,
                          fillColor:
                              MaterialStateProperty.resolveWith(getColor),
                          value: mapProvider.bencineras,
                          onChanged: (bool? value) {
                            mapProvider.bencineras = value!;
                          }),
                      Text(
                        mapProvider.nombreBencineras,
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Image.asset(
                        mapProvider.imgBancos,
                        width: 30.0,
                      ),
                      Checkbox(
                          checkColor: Colors.white,
                          fillColor:
                              MaterialStateProperty.resolveWith(getColor),
                          value: mapProvider.bancos,
                          onChanged: (bool? value) {
                            mapProvider.bancos = value!;
                          }),
                      Text(
                        mapProvider.nombreBancos,
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Image.asset(
                        mapProvider.imgBanios,
                        width: 30.0,
                      ),
                      Checkbox(
                          checkColor: Colors.white,
                          fillColor:
                              MaterialStateProperty.resolveWith(getColor),
                          value: mapProvider.banios,
                          onChanged: (bool? value) {
                            mapProvider.banios = value!;
                          }),
                      Text(
                        mapProvider.nombreBanios,
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Image.asset(
                        mapProvider.imgFarmacias,
                        width: 30.0,
                      ),
                      Checkbox(
                          checkColor: Colors.white,
                          fillColor:
                              MaterialStateProperty.resolveWith(getColor),
                          value: mapProvider.farmacias,
                          onChanged: (bool? value) {
                            mapProvider.farmacias = value!;
                          }),
                      Text(
                        mapProvider.nombreFarmacias,
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Image.asset(
                        mapProvider.imgSalud,
                        width: 30.0,
                      ),
                      Checkbox(
                          checkColor: Colors.white,
                          fillColor:
                              MaterialStateProperty.resolveWith(getColor),
                          value: mapProvider.salud,
                          onChanged: (bool? value) {
                            mapProvider.salud = value!;
                          }),
                      Text(
                        mapProvider.nombreSalud,
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Image.asset(
                        mapProvider.imgFerreteria,
                        width: 30.0,
                      ),
                      Checkbox(
                          checkColor: Colors.white,
                          fillColor:
                              MaterialStateProperty.resolveWith(getColor),
                          value: mapProvider.ferreteria,
                          onChanged: (bool? value) {
                            mapProvider.ferreteria = value!;
                          }),
                      Text(
                        mapProvider.nombreFerreteria,
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Image.asset(
                        mapProvider.imgPolicia,
                        width: 30.0,
                      ),
                      Checkbox(
                          checkColor: Colors.white,
                          fillColor:
                              MaterialStateProperty.resolveWith(getColor),
                          value: mapProvider.policia,
                          onChanged: (bool? value) {
                            mapProvider.policia = value!;
                          }),
                      Text(
                        mapProvider.nombrePolicia,
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Image.asset(
                        mapProvider.imgSupermercado,
                        width: 30.0,
                      ),
                      Checkbox(
                          checkColor: Colors.white,
                          fillColor:
                              MaterialStateProperty.resolveWith(getColor),
                          value: mapProvider.supermercado,
                          onChanged: (bool? value) {
                            mapProvider.supermercado = value!;
                          }),
                      Text(
                        mapProvider.nombreSupermercado,
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Image.asset(
                        mapProvider.imgWifi,
                        width: 30.0,
                      ),
                      Checkbox(
                          checkColor: Colors.white,
                          fillColor:
                              MaterialStateProperty.resolveWith(getColor),
                          value: mapProvider.wifi,
                          onChanged: (bool? value) {
                            mapProvider.wifi = value!;
                          }),
                      Text(
                        mapProvider.nombreWifi,
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                ],
              ),
            )
          : Center(),
    ],
  );
}

Widget grupoCheckBox(MapProvider mapProvider, getColor, context) {
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: Column(
      children: [
        checkBoxQueHacer(mapProvider, getColor, context),
        checkBoxDondeDormir(mapProvider, getColor, context),
        checkBoxDondeComer(mapProvider, getColor, context),
        checkBoxDatosUtiles(mapProvider, getColor, context),
      ],
    ),
  );
}

//TODO CREAR DRAWER
Widget _invitado(context, MapProvider mapProvider) {
  final authService = Provider.of<AuthService>(context);
  final size = MediaQuery.of(context).size;
  return Column(
    children: [
      Container(
        width: size.width * 0.6,
        child: Text('Perfil Invitado',
            style: TextStyle(color: Colors.white, fontSize: 20.0)),
      ),
      Row(
        children: [
          Divider(
            color: Colors.white,
            thickness: 2,
            indent: 5,
            endIndent: 5,
          ),
        ],
      ),
      Container(
        width: size.width * 0.6,
        child: Text(
            'Hola Viajero! Puedes seleccionar solo lo que buscas y ver reflejado en el mapa',
            style: TextStyle(color: Colors.white)),
      ),
      grupoCheckBox(mapProvider, getColor, context),
      GFListTile(
        color: Colors.white,
        titleText: 'Iniciar Sesion',
        avatar: Icon(Icons.logout_rounded),
        icon: Icon(Icons.arrow_forward_ios_outlined),
        onTap: () async {
          await authService.logout();
          Future.microtask(() => {
                Navigator.pushReplacement(
                    context,
                    PageRouteBuilder(
                      pageBuilder: (_, __, ___) => CheckAuthScreen(),
                      transitionDuration: Duration(seconds: 1),
                    )),
              });
        },
      ),
      Text(
        'www.living.cl',
        style: TextStyle(
          color: Colors.white,
        ),
      ),
    ],
  );
}

Widget _session(context, MapProvider mapProvider) {
  final authService = Provider.of<AuthService>(context);
  final size = MediaQuery.of(context).size;
  return Column(children: [
    grupoCheckBox(mapProvider, getColor, context),
    Divider(
      color: Colors.white,
    ),
    ListTile(
      title: Text(
        "Configuracion",
        style: TextStyle(color: Colors.white),
      ),
    ),
    GFListTile(
      titleText: 'Configuracion',
      color: Colors.white,
      avatar: Icon(Icons.settings_applications_outlined),
      icon: Icon(Icons.arrow_forward_ios_outlined),
      onTap: () async {
        Future.microtask(() => {
              Navigator.pushNamed(context, 'user_configuracion'),
            });
      },
    ),
    GFListTile(
      color: Colors.white,
      titleText: 'Cerrar Sesion',
      avatar: Icon(Icons.logout_rounded),
      icon: Icon(Icons.arrow_forward_ios_outlined),
      onTap: () async {
        await authService.logout();
        Future.microtask(() => {
              Navigator.pushReplacement(
                  context,
                  PageRouteBuilder(
                    pageBuilder: (_, __, ___) => CheckAuthScreen(),
                    transitionDuration: Duration(seconds: 1),
                  )),
            });
      },
    ),
  ]);
}

Widget _drawer(context, MapProvider mapProvider) {
  final authService = Provider.of<AuthService>(context);
  final size = MediaQuery.of(context).size;

  return GFDrawer(
    color: Colors.black87,
    child: ListView(
      padding: EdgeInsets.zero,
      children: <Widget>[
        Container(
          height: 200,
          width: 200,
          alignment: Alignment.center,
          child: Stack(
            children: [
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 40),
                    child: Image.asset("assets/logo_blanco.png",
                        fit: BoxFit.cover, width: size.width * 0.2),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(40.0),
                    child: Image.asset("assets/living_titulo.png",
                        fit: BoxFit.cover, width: size.width * 0.25),
                  )
                ],
              ),
            ],
          ),
        ),
        FutureBuilder(
            future: Future.wait([
              authService.esInvitado(),
            ]),
            builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
              if (snapshot.hasData) {
                if (snapshot.data![0] == 'true') {
                  return _invitado(context, mapProvider);
                } else {
                  return _session(context, mapProvider);
                }
              }
              return Container();
            }),
      ],
    ),
  );

  /*return SafeArea(
    child: Drawer(
      child: Container(
        width: 300,
        child: Container(
          padding: EdgeInsets.only(top: 30) ,
          child: Column(
            children: [
            ListTile(
              onTap: () async {
                await authService.logout();
                Future.microtask(() => {
                  Navigator.pushReplacement(context, PageRouteBuilder(
                    pageBuilder: (_ , __ , ___ ) => CheckAuthScreen(),
                    transitionDuration: Duration( seconds: 1),
                  )),
                });
              },
              title: Text('CERRAR SESION'),
            )
            ],
          ),
        ),
      ),
    ),
  );*/
}

