import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:parque/Util/CONSTANTES.dart';
import 'package:parque/servicios/servicios.dart';
import 'package:provider/provider.dart';

class CarouselWithIndicatorDemo extends StatefulWidget {
  final int id_empresa;
   List<Widget> contenedorFotos = [];
  bool existen_fotos = true;
  CarouselWithIndicatorDemo({Key? key, required this.id_empresa}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return _CarouselWithIndicatorState();
  }
}

class _CarouselWithIndicatorState extends State<CarouselWithIndicatorDemo> {
  int _current = 0;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final authService = Provider.of<AuthService>(context , listen: false);
    contenedorFotos(authService);
    if(widget.contenedorFotos.isEmpty){
      if(widget.existen_fotos){
        return Center(child: CircularProgressIndicator());

      }else {
        return CarouselSlider(
          items: [ Image.asset('assets/no-image.jpg' , fit: BoxFit.fitWidth, width: size.width * 1,)],
          options: CarouselOptions(
              height: 200.0,
              enableInfiniteScroll: false,
              autoPlay: false,
              enlargeCenterPage: true,
              aspectRatio: 4 / 3,

         ),
        );
      }
    }else {
      return CarouselSlider(
        items: widget.contenedorFotos,
        options: CarouselOptions(
            height: 200.0,
            enableInfiniteScroll: true,
            autoPlay: true,
            enlargeCenterPage: true,
            aspectRatio: 4 / 3,
            onPageChanged: (index, reason) {
              setState(() {
                _current = index;
              });
            }),
      );
    }
    // Row(
    //   mainAxisAlignment: MainAxisAlignment.center,
    //   children: imgList.map((url) {
    //     int index = imgList.indexOf(url);
    //     return Container(
    //       width: 8.0,
    //       height: 8.0,
    //       margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
    //       decoration: BoxDecoration(
    //         shape: BoxShape.circle,
    //         color: _current == index
    //             ? Color.fromRGBO(0, 0, 0, 0.9)
    //             : Color.fromRGBO(0, 0, 0, 0.4),
    //       ),
    //     );
    //   }).toList(),
    // ),
  }

  Future<void> contenedorFotos(AuthService authService )  async {

    List<String> fotos =[];
    if(widget.contenedorFotos.isEmpty && widget.existen_fotos) {
      var respuesta = await obtenerImagenes(authService);

      List<Container> lista = [];
      var urls = respuesta['data'] as List;
      urls.forEach((element) {

        if(element != null){
          var url = 'http://'+ CONSTANTES.BASEURL + element;
        lista.add(
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(0.0),
                image: DecorationImage(
                  image: NetworkImage(url),
                  fit: BoxFit.contain,
                ),
              ),
            ));
      }});

      if(urls.isNotEmpty){
        setState(() {
          widget.contenedorFotos = lista;

        });
      }
      if(respuesta['existe'] == false){
        setState(() {
          widget.existen_fotos = false;
        });
      }else{
        widget.existen_fotos = true;
      }


    }
  }
  Future<dynamic> obtenerImagenes(AuthService authService) async {

    var respuesta = await authService.obtenerImagenesEmpresa(widget.id_empresa);
    return respuesta;
  }
}
