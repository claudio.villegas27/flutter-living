import 'package:flutter/material.dart';
import 'package:parque/providers/map_provider.dart';
import 'package:parque/screens/guest/auth.dart';
import 'package:parque/screens/user/empresa_screen.dart';
import 'package:parque/screens/user/user.dart';
import 'package:parque/servicios/living_service.dart';
import 'package:parque/servicios/servicios.dart';
import 'package:provider/provider.dart';

void main() => runApp(AppState());

class AppState extends StatelessWidget {
  const AppState({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(providers: [
          ChangeNotifierProvider(create: (_) => AuthService()),
          ChangeNotifierProvider(create: (_) => LivingService()),
          ChangeNotifierProvider(create: (_) => MapProvider())


    ],
    child: MyApp(),
    );

  }
}


class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    //final authService = Provider.of<AuthService>(context , listen: false);

          // Loading is done, return the app:
          return MaterialApp(
            debugShowCheckedModeBanner: true,
            // ignore: unrelated_type_equality_checks
            initialRoute: 'checkAuth',  //TODO DEJAR CHECKAUTH LUEGO DE TESTEAR
            routes: {
              'home': (_) => HomeScreen(),
              'registro': (_) => RegisterScreen(),
              'login': (_) => LoginScreen(),
              'user_home' :(_) => UserHomeScreen(),
              'checkAuth':(_) => CheckAuthScreen(),
              'user_configuracion':(_) => ConfiguracionScreen(),

            },
            theme: new ThemeData(scaffoldBackgroundColor: Colors.white ,  textTheme: const TextTheme(
              headline1: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold , backgroundColor: Colors.white),
              headline6: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
              bodyText2: TextStyle(fontSize: 14.0, fontFamily: 'Hind'),
            ), ) ,
          );

  }
}
class Splash extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: Center(
        child: Icon(
          Icons.apartment_outlined,
          size: MediaQuery.of(context).size.width * 0.785,
        ),
      ),
    );
  }
}