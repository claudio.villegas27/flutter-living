import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:parque/Util/CONSTANTES.dart';
import 'package:parque/models/models.dart';
import 'package:parque/models/servicios_model.dart';
import 'package:parque/providers/db_provider.dart';
class AuthService extends ChangeNotifier{

  final String _baseUrl= CONSTANTES.BASEURL;
  final storage = new FlutterSecureStorage();
  List<Servicios> listaServicios= [];
  late final Servicios informacion_empresa;





  Future<Map<String, dynamic>?> crearUsuario(String rut , String email , String password ) async {

    final Map<String , dynamic> authData = {
      'rut' :rut,
      'email':email,
      'password':password,
    };

    final url =  Uri.http(_baseUrl,'/api/registro/app');
    final resp = await http.post(url , body: authData);
    final Map<String,dynamic>decodeResp = json.decode(resp.body);

      return decodeResp;

  }
  void crearInvitado() async {
     await storage.write(key: "invitado", value: 'true');
  }
  Future<String> esInvitado() async{
    return await storage.read(key:'invitado') ?? 'false';
  }
  Future<Map<String, dynamic>?> loginUsuario(String email , String password ) async {
    try {
      final Map<String, dynamic> authData = {
        'email': email,
        'password': password,
      };

      final url = Uri.http(_baseUrl, '/api/login');
      final resp = await http.post(url , body: authData);
      print(resp);
      final Map<String, dynamic>decodeResp = json.decode(resp.body);
      if (decodeResp.containsKey('data')) {
        //TODO TEMPORAL
        final usuario_json = Usuario.fromJson(decodeResp['data']['usuario']);
        print(usuario_json);
        final usuario = DBProvider.getUser(usuario_json.id_usuario);
        // ignore: unnecessary_null_comparison
        if(usuario == null) {
          DBProvider.insertUser(usuario_json);
        }

        await storage.write(key: "logged", value: 'true');
        await storage.write(key: "id_usuario", value: decodeResp['data']['usuario']['id_usuario'].toString());

      }
      return decodeResp;
    }on SocketException {
      print("error SocketException");
    }on HttpException {
      print("Couldn't find the post 😱");
    }on FormatException {
      print("Bad response format 👎 loginUsuario");
    }
  }
  Future<String> isLogged() async{
    return await storage.read(key:'logged') ?? '';
  }

  Future<bool> logout() async{
    await storage.deleteAll();

    return true;
  }

  Future<Usuario?> obtenerUsuario() async{
     // ignore: non_constant_identifier_names
     final id_usuario = await storage.read(key: 'id_usuario');
     return DBProvider.getUser(id_usuario);

  }




  Future<List<Servicios>?> obtenerServicios() async {
    try {
      final url = Uri.http(_baseUrl, '/api/getServicios');
      final resp = await http.get(url);
      final Map<String, dynamic>decodeResp = json.decode(resp.body);
      return listaServicios = List<Servicios>.from(decodeResp['data'].map((x) => Servicios.fromMap(x)));

    }on SocketException {
      print("error SocketException");
    }on HttpException {
      print("Couldn't find the post 😱");
    }on FormatException {
      print("Bad response format 👎2 obtenerServicios");
    }
  }
  Future<Servicios?> obtenerInfoEmpresa(id) async {
    try {

      final url = Uri.http(_baseUrl, '/api/getServicios' , {
        'id_empresa':id
      });
      final resp = await http.get(url);
      final Map<String, dynamic>decodeResp = json.decode(resp.body);

      return informacion_empresa = Servicios.fromMap(decodeResp['data']);


    }on SocketException {
      print("error SocketException");
    }on HttpException {
      print("Couldn't find the post 😱");
    }on FormatException {
      print("Bad response format 👎2 obtenerInfoEmpresa");
    }
  }

  Future<Map<String, dynamic>?> obtenerImagenesEmpresa(id) async {
    try {

      final url = Uri.http(_baseUrl, '/api/getImagenesEmpresa' , {
        'id_empresa':'${id}'
      });
      final resp = await http.get(url);
      final Map<String, dynamic>decodeResp = json.decode(resp.body);

      return decodeResp;

    }on SocketException {
      print("error SocketException");
    }on HttpException {
      print("Couldn't find the post 😱");
    }on FormatException {
      print("Bad response format 👎2 obtenerImagenesEmpresa");
    }
  }









}