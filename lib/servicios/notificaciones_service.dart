import 'package:flutter/material.dart';

class NotificacionesService{

  static late GlobalKey<ScaffoldMessengerState> messengerKey  = new GlobalKey<ScaffoldMessengerState>();


  static showSnackBar(String message){
      final snackBar = new SnackBar(content: Text(message));

      messengerKey.currentState!.showSnackBar(snackBar);
  }

}